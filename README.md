
# Combining Voxel and Normal Predictions for Multi-View 3D Sketching

This is the code for the paper

**Combining Voxel and Normal Predictions for Multi-View 3D Sketching**. Johanna Delanoy, David Coeurjolly, Jacques-Olivier Lachaud, Adrien Bousseau.  Computers and Graphics, Elsevier, 2019, Proceedings of Shape Modeling International 2019. [Offical published version](https://www.sciencedirect.com/science/article/pii/S0097849319300858), [author's version](https://hal.archives-ouvertes.fr/hal-02141469/document) .



## Installation

To download and install dependencies, run
```
./install.sh
```

To build the code, run
```
./compile.sh
```

## Replicate results from the paper

Use the scripts `figX.sh` to get the meshes used for the corresponding figure in the paper. Read the first line of comments in the script to know where to find the right meshes.