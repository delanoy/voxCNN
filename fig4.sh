#fig4 a) : test_data/camera_X_clean.png , test_data/camera_X_normals.png and fig4/camera_initialSurf.obj
#fig4 b) AT alpha=0.02 : fig4/camera_0_02_regularizedSurf_AT.obj
#fig4 c) AT alpha=0.05 : fig4/camera_0_05_regularizedSurf_AT.obj
#fig4 d) AT alpha=0.1 : fig4/camera_0_1_regularizedSurf_AT.obj
#fig4 e) : fig4/camera_regularizedSurf_WAT.obj

#get normal maps and volumetric prediction
./cnn_prediction.sh test_data/camera

mkdir fig4/
./build/regularize_AT_only test_data/camera fig4/camera_0_02 0.02
./build/regularize_AT_only test_data/camera fig4/camera_0_05 0.05
./build/regularize_AT_only test_data/camera fig4/camera_0_1 0.1
./build/regularize_WAT_only test_data/camera fig4/camera

