#fig8 column ours: fig8/X_name_regularizedSurf_WAT.obj
#with X the number of the line in the paper, name is the name of the object in test_data

objects=(lwg plane_triangle2 toothbrush3)
mkdir fig8

for i in {0..2}
do
    #get normal maps and volumetric prediction
    ./cnn_prediction.sh test_data/${objects[$i]}
    ./build/regularize_WAT_only test_data/${objects[$i]} fig8/$i"_"${objects[$i]}
done
