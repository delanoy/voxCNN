#fig5 column ours: fig5/X_NAME_regularizedSurf_WAT.obj
#with X the number of the line in the paper, NAME is the name of the object in test_data
#input sketches in test_data/NAME_V_clean.png (V the view number)
#normal maps in test_data/NAME_V_normals.png (V the view number)

objects=(CHAIR armchair1 WAGON_03 plane1 wagon_hole_06 house_4views)
mkdir fig5

for i in {0..5}
do
    #get normal maps and volumetric prediction
    ./cnn_prediction.sh test_data/${objects[$i]}
    ./build/regularize_WAT_only test_data/${objects[$i]} fig5/$i"_"${objects[$i]}
done
