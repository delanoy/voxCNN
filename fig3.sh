#objects from left to right in the picture
#fig3 input: fig1_2_3/armchair_2views_initialSurf.obj
#fig3 n_g : fig1_2_3/armchair_2views_regularizedSurf_grad.obj
#fig3 n_g+AT : fig1_2_3/armchair_2views_regularizedSurf_grad_AT.obj
#fig3 n_bar : fig1_2_3/armchair_2views_regularizedSurf_mean.obj
#fig3 n_bar+AT : fig1_2_3/armchair_2views_regularizedSurf_AT.obj
#fig3 n_bar+weighted AT : fig1_2_3/armchair_2views_regularizedSurf_WAT.obj

#get normal maps and volumetric prediction
./cnn_prediction.sh test_data/armchair_2views

mkdir fig1_2_3/
./build/regularize_all_outputs test_data/armchair_2views fig1_2_3/armchair_2views
