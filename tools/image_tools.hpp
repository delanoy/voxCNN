#ifndef _IMAGE_TOOLS
#define _IMAGE_TOOLS

#include <opencv2/core.hpp>
//#include <cv.h>
#include <random>
#include <functional>


cv::Mat resize(const cv::Mat &img, cv::Size size);
cv::Mat compute_gradient(const cv::Mat &input);

//split rgb channels of an opencv image to store each channel in a consecutive manner
template <typename T>
std::vector<cv::Mat> split_channels(const cv::Mat& img, T* data=NULL);

cv::Mat merge_channels_u(unsigned char* data, int nrows, int ncols);
cv::Mat merge_channels_ua(unsigned char* data, int nrows, int ncols);
cv::Mat merge_channels_f(float* data, int nrows, int ncols);

cv::Mat convert_to_float(const cv::Mat& img, bool divide = true);
cv::Mat convert_to_uint(const cv::Mat& img, bool scale=true);

cv::Mat remove_alpha(const cv::Mat& img);
cv::Mat take_alpha(const cv::Mat& img);
cv::Mat flip(const cv::Mat& img);
cv::Mat repeat_channel(const cv::Mat& img, int channel, int nb);
cv::Mat inverse_B_R(const cv::Mat& img);

cv::Mat superimpose_sketch(const cv::Mat &img, const cv::Mat &sketch,
						   const cv::Vec3b &color = cv::Vec3b(0,0,0), float factor = 0.5);

void launch_in_parallel(std::function<void(int,int)> & function, int nthreads, int npoints);

cv::Vec3b jetColor(float gray);

cv::Mat dilate_foreground(cv::Mat img, bool(&is_background)(cv::Vec3b));
#endif
