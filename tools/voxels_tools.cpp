#include "voxels_tools.hpp"

#include "rotation_tools.hpp"
#include "convolution_kernel.hpp"

#include <image_tools.hpp>
//#include <igl/copyleft/marching_cubes.h>
#include <iostream>
#include <opencv2/imgproc.hpp>

#include <opencv2/core.hpp>
#include <thread>
#include <fstream>
#include <opencv2/highgui.hpp>


void VoxelGrid::gaussian_noise(float intensity)
{
    std::default_random_engine generator;
    std::normal_distribution<double> distribution(0.0,intensity);

	VoxelGrid new_vox(size());
	for(int c = 0; c < size(); c++)
	{
		for(int i = 0; i <size(); i++)
		{
			for(int j = 0; j < size(); j++)
			{
				if((*this)(i,j,c)<0.05) continue;
                double noise = distribution(generator);
                (*this)(i,j,c) += noise;
			}
		}
	}
}


VoxelGrid VoxelGrid::binarize(float threshold) const
{
	VoxelGrid new_vox(size());
	for(int c = 0; c < size(); c++)
	{
		for(int i = 0; i <size(); i++)
		{
			for(int j = 0; j < size(); j++)
			{

				if((*this)(i,j,c) > threshold)
					new_vox(i,j,c) = 1;
				else
					new_vox(i,j,c) = 0;
			}
		}
	}
	return new_vox;
}

cv::Mat VoxelGrid::pack_in_image( int grid_rows, int grid_cols)
{
	assert(grid_rows*grid_cols == size()/4);
	int nchannels = size()/4;//std::min<int>(grid.size(), grid_rows*grid_cols);
	int nrows = size();
	int ncols = size();
	//cv::Mat img(nrows*grid_rows, ncols*grid_cols,CV_8UC4);
	std::vector<cv::Mat> imgs;

	for(int i = 0; i<4; i++)
	{
		imgs.push_back(cv::Mat(nrows*grid_rows, ncols*grid_cols,CV_8UC1));
	}

	for(int c = 0; c<nchannels; c++)
	{
		for(int i = 0; i<4; i++)
		{
			int channel = c*4 + i;
			int i_cell = c/grid_cols*nrows;
			int j_cell = c%grid_cols*ncols;

			cv::Mat slice(nrows,ncols,CV_32FC1, (this->slice(channel)).data()), sliceU;
			slice.convertTo(sliceU, CV_8U, 255);
			sliceU.copyTo(imgs[i](cv::Rect(j_cell,i_cell,nrows,ncols)));
//			cv::imshow("slice",slice);
//			cv::imshow("sliceU",sliceU);
//			cv::imshow("channel",imgs[i]);
//			cv::waitKey();
		}
	}
	int from_to[4*2] = {0,2,1,1,2,0,3,3};
	std::vector<cv::Mat> img(1,cv::Mat(nrows*grid_rows, ncols*grid_cols,CV_8UC4));
	cv::mixChannels(imgs,img,from_to,4);
	//cv::merge(imgs,img);
	return img[0];
}

VoxelGrid VoxelGrid::unpack_from_image( cv::Mat &image, int grid_rows, int grid_cols)
{
	//assert(grid_rows*grid_cols == grid.size()/4);
	int nrows = image.rows/grid_rows;
	int ncols = image.cols/grid_cols;
	int nchannels = nrows/4;//std::min<int>(grid.size(), grid_rows*grid_cols);
	//std::cout<<"start to unpack\n";
	std::vector<cv::Mat> imgs;
	int from_to[4*2] = {0,2,1,1,2,0,3,3};
	std::vector<cv::Mat> img;
	img.push_back(image);
	// std::cout<<img[0].type()<<std::endl;
	// std::cout<<img[0].channels()<<std::endl;
	for(int i = 0; i<4; i++)
	{
		imgs.push_back(cv::Mat(image.size(),CV_8UC1));
	}
	cv::mixChannels(img,imgs,from_to,4);

	VoxelGrid grid(nrows);

	for(int c = 0; c<nchannels; c++)
	{
		int i_cell = c/grid_cols*nrows;
		int j_cell = c%grid_cols*ncols;
		//std::cout<<"c = "<<c<<", icell = "<<i_cell<<", jcell = "<<j_cell<<std::endl;
		for(int i = 0; i<4; i++)
		{
			int channel = c*4 + i;
			cv::Mat part = imgs[i](cv::Rect(j_cell,i_cell,nrows,ncols)),partF;
			part.convertTo(partF, CV_32F, 1.0/255);

			Eigen::Map<FloatSlice> chan(reinterpret_cast<float*>(partF.data),nrows,ncols);
			grid.slice(channel) =chan;

		}
	}
	// cv::imshow("grid",pred_to_grid(grid,8,8));
	// cv::waitKey(0);
	return grid;
}



VoxelGrid VoxelGrid::rotate_voxels(const Eigen::Matrix4f &model,
				   const Eigen::Matrix4f &view,
				   const Eigen::Matrix4f &proj, int size_ratio,bool interpolate) const
{
	int size = this->size()*size_ratio;
	VoxelGrid rot_vox(size);
	//for(int c = 0; c < size; c++)
	auto func = [&](int c)
	{
		for(int i = 0; i <size; i++)
		{
			for(int j = 0; j < size; j++)
			{
				rot_vox(i,j,c) = rotated_voxel_value(*this, model, view, proj, c, i, j, size_ratio, interpolate);
			}
		}
	};
	std::vector<std::thread> th;
	for(int c = 0; c < size; c++)
		th.push_back(std::thread(func, c));
	for(auto &t : th)
		t.join();


	return rot_vox;

}

//rotate a voxel grid from cam1 to cam2
VoxelGrid VoxelGrid::rotate_voxels_prediction(const Eigen::Matrix4f &model1,
								   const Eigen::Matrix4f &model2,
								   const Eigen::Matrix4f &view1,
								   const Eigen::Matrix4f &view2,
								   const Eigen::Matrix4f &proj, int size_ratio,bool interpolate) const
{
	int size = this->size() * size_ratio;
	VoxelGrid rot_vox(size);
	//for(int c = 0; c < size; c++)
	auto func = [&](int c)
	{
		for(int i = 0; i <size; i++)
		{
			for(int j = 0; j < size; j++)
			{
				rot_vox(i,j,c) = rotated_proba_value(*this, model1, model2, view1, view2, proj, c, i, j, size_ratio, interpolate);
			}
		}
	};

		std::vector<std::thread> th;
	for(int c = 0; c < size; c++)
		th.push_back(std::thread(func, c));
	for(auto &t : th)
		t.join();
	return rot_vox;

}

//rotate a voxel grid from cam1 to cam2
VoxelGrid VoxelGrid::rotate_voxels_prediction(const Eigen::Matrix4f &model,
								   const Eigen::Matrix4f &view,
								   const Eigen::Matrix4f &proj, int size_ratio,bool interpolate) const
{
	return rotate_voxels_prediction(Eigen::Matrix4f::Identity(),model,view,view,proj,size_ratio,interpolate);
}


VoxelGrid VoxelGrid::unproject_voxels_prediction(const Eigen::Matrix4f &view,
									  const Eigen::Matrix4f &proj, int size_ratio,bool interpolate) const
{
	int size = this->size()*size_ratio;
	VoxelGrid can_vox(size);
	for(int c = 0; c < size; c++)
	{
		//int i = 30;
		for(int i = 0; i <size; i++)
		{
			//int j = 30;
			for(int j = 0; j < size; j++)
			{
				can_vox(i,j,c) = unproject_voxel_value(*this, view, proj, c, i, j, size_ratio, interpolate);
			}
		}
	}
	return can_vox;

}


VoxelGrid VoxelGrid::project_voxels_prediction(const Eigen::Matrix4f &model,const Eigen::Matrix4f &view,
									  const Eigen::Matrix4f &proj, int size_ratio,bool interpolate) const
{
	int size = this->size()*size_ratio;
	VoxelGrid can_vox(size);
	for(int c = 0; c < size; c++)
	{
		//int i = 30;
		for(int i = 0; i <size; i++)
		{
			//int j = 30;
			for(int j = 0; j < size; j++)
			{
				can_vox(i,j,c) = project_voxel_value(*this, model, view, proj, c, i, j, size_ratio, interpolate);
			}
		}
	}
	return can_vox;

}





cv::Mat VoxelGrid::pred_to_grid(int grid_rows, int grid_cols, int begin)
{
	int nchannels = std::min<int>(size()-begin, grid_rows*grid_cols);
	int nrows = size();
	int ncols = size();
	cv::Mat img(nrows*grid_rows, ncols*grid_cols,CV_32FC1);
	//std::cout<<img.size()<<std::endl;
	for(int c = 0; c<nchannels; c++)
	{
		int i_cell = c/grid_cols*nrows;
		int j_cell = c%grid_cols*ncols;

		for(int i = 0; i<nrows; i++)
		{
			for(int j = 0; j<ncols; j++)
			{
				img.at<float>(i_cell+i, j_cell+j)=(*this)(i,j,c+begin);
			}
		}
	}
	return img;

}










std::vector<VoxelGrid> VoxelGrid::compute_gradient() const
{
	int size = this->size();
	std::vector<VoxelGrid> res (4);
	for (int i = 0; i < 4; i++)
		res[i] = VoxelGrid(size);

	auto func = [&](int c)  {
		for(int i = 0; i <size; i++)
		{
			for(int j = 0; j < size; j++)
			{
				// std::cout<<grid[c-1].block(i-1, j-1, 3, 3)<<std::endl;
				// std::cout<<grid[c+1].block(i-1, j-1, 3, 3)<<std::endl;
				Eigen::RowVector3f G;
				G.x() = 0.5*((*this)(coord(i-1), j,c)-(*this)(coord(i+1), j,c));
				G.y() = 0.5*((*this)(i, coord(j-1),c)-(*this)(i, coord(j+1),c));
				G.z() = 0.5*((*this)(i,j,coord(c-1))-(*this)(i,j,coord(c+1)));
				// std::cout<<G<<std::endl;

				res[3](i,j,c) = G.norm()<1e-5?1:G.norm();
				res[0](i,j,c) = G.x()/res[3](i,j,c);///2+0.5;
				res[1](i,j,c) = G.y()/res[3](i,j,c);///2+0.5;
				res[2](i,j,c) = G.z()/res[3](i,j,c);///2+0.5;
				// std::cout<<res[c](i,j)<<std::endl;
			}
		}
	};

	std::vector<std::thread> th;
	for(int c = 0; c < size; c++)
		th.push_back(std::thread(func, c));
	for(auto &t : th)
		t.join();

	return res;

}

VoxelGrid VoxelGrid::blur() const
{
	return  convolution_3d(gaussian_kernel_3d());
}

cv::Mat VoxelGrid::normal_map(float threshold, bool blur) const
{
	std::vector<VoxelGrid> norm;
	if(blur) {
        norm = convolution_3d(gaussian_kernel_3d()).compute_gradient();
    }
	else
		norm = compute_gradient();
	return normal_map(threshold, norm);
}

cv::Mat VoxelGrid::iso_surface(float threshold, bool colorize) const
{
	int size = this->size();
	cv::Mat img(size, size,colorize?CV_8UC3:CV_32FC1);
	for(int i = 0; i <size; i++)
	{
		for(int j = 0; j < size; j++)
		{
			// std::cout<<i<<" "<<j<<<
			int c_depth = -1;
			for(int c = 0; c < size; c++)
			{
				float val_depth= (*this)(i,j,c);
				if (val_depth > threshold)
				{
					c_depth = c;
					break;
				}
			}
			float zf=c_depth*0.8+(size/16);
			float depth = -(zf)/size*5.5-2.5;
            depth = 7.27273/depth+1.90909;
            depth=depth/2+0.5;
            if(colorize)
            {
                cv::Vec3b color = jetColor(depth*2-1);
                img.at<cv::Vec3b>(i, j)=color;
            } else{
                img.at<float>(i, j)=depth;
            }
		}
	}

	return img;

}

cv::Mat VoxelGrid::normal_map(float threshold,
				   std::vector<VoxelGrid> &norm) const
{
	int size = this->size();
	cv::Mat img(size, size,CV_8UC3);
	for(int i = 0; i <size; i++)
	{
		for(int j = 0; j < size; j++)
		{
			// std::cout<<i<<" "<<j<<<
			int c_depth = -1;
			for(int c = 0; c < size; c++)
			{
				float val_depth= (*this)(i,j,c);
				if (val_depth > threshold)
				{
					c_depth = c;
					break;
				}
			}
			if (c_depth >= 0)
				img.at<cv::Vec3b>(i, j)=cv::Vec3b((-(norm[2](i,j,c_depth)/2+0.5)*255),
												  ((-norm[0](i,j,c_depth)/2+0.5)*255),
												  ((norm[1](i,j,c_depth)/2+0.5)*255));
			else
				img.at<cv::Vec3b>(i, j)=cv::Vec3b(127,127,127);

		}
	}

	return img;
}




VoxelGrid VoxelGrid::convolution_3d(const VoxelGrid &kernel) const
{
	int size = this->size();
	int k_shift = kernel.size()/2;
	VoxelGrid res(size,0);

	// std::cout <<"================== KERNEL =================\n";
	// std::cout<<kernel[k_shift]/kernel[k_shift](k_shift,k_shift)<<std::endl<<std::endl;

	auto func = [&](int c)  {
		for(int i = 0; i <size; i++)
		{
			for(int j = 0; j < size; j++)
			{
				for(int dc = -k_shift; dc <= k_shift; dc ++)
					for(int di = -k_shift; di <= k_shift; di ++)
						for(int dj = -k_shift; dj <= k_shift; dj ++)
							res(i,j,c) += (*this)(coord(i+di),coord(j+dj),coord(c+dc))
								*kernel(di+k_shift, dj+k_shift,dc+k_shift);

			}
		}
	};

	std::vector<std::thread> th;
	for(int c = 0; c < size; c++)
		th.push_back(std::thread(func, c));
	for(auto &t : th)
		t.join();

	return res;
}








