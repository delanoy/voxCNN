#include "image_tools.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <thread>

cv::Mat compute_gradient(const cv::Mat &input)
{
	cv::Mat src_gray,grad;
	/// Convert it to gray
	if(input.channels() > 1)
		cv::cvtColor( input, src_gray, CV_BGR2GRAY );
	else
		src_gray=input;
	/// Generate grad_x and grad_y
	cv::Mat grad_x, grad_y;
	cv::Mat abs_grad_x, abs_grad_y;
	int scale = 1;
	int delta = 0;
	int ddepth = CV_32F;
	/// Gradient X
	//Scharr( src_gray, grad_x, ddepth, 1, 0, scale, delta, BORDER_DEFAULT );
	cv::Sobel( src_gray, grad_x, ddepth, 1, 0, 3, scale, delta, cv::BORDER_DEFAULT );
	cv::convertScaleAbs( grad_x, abs_grad_x ,(input.type()%8 < 3) ? 1 : 255);

	/// Gradient Y
	//Scharr( src_gray, grad_y, ddepth, 0, 1, scale, delta, BORDER_DEFAULT );
	cv::Sobel( src_gray, grad_y, ddepth, 0, 1, 3, scale, delta, cv::BORDER_DEFAULT );
	cv::convertScaleAbs( grad_y, abs_grad_y ,(input.type()%8 < 3) ? 1 : 255);
	/// Total Gradient (approximate)
	cv::addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad );
	cv::threshold(grad,grad,0.5*255,255, cv::THRESH_TOZERO_INV);
	return grad;
}

cv::Mat superimpose_sketch(const cv::Mat &img, const cv::Mat &sketch,
						   const cv::Vec3b &color, float factor )
{
	cv::Mat res;// = img;
	cv::resize(img, res, sketch.size(),0,0,cv::INTER_NEAREST);
	for(int i = 0; i <res.rows; i++)
		{
			for(int j = 0; j < res.cols; j++)
			{
				if(sketch.at<cv::Vec3b>(i,j)[0]<250)
				{
					res.at<cv::Vec3b>(i,j) = color*factor + res.at<cv::Vec3b>(i,j)*(1.0-factor);
				}
			}
		}
	return res;
}

cv::Mat resize(const cv::Mat &img, cv::Size size)
{
	cv::Mat res;
	cv::resize(img, res, size);
	return res;
}


template <typename T>
std::vector<cv::Mat> split_channels(const cv::Mat& img, T* data)
{
	
	if (data == NULL)
	{
		data = new T[img.channels()*img.rows*img.cols];
	}
	//cv::Mat rgb( img.rows, img.cols, img.type());
	//int from_to[] = { 0,2, 1,1, 2,0};
	//cv::mixChannels(&img, 1, &rgb, 1, from_to, 3);
	std::vector<cv::Mat> channels;
	for (int i = 0; i < img.channels(); ++i) {
	  cv::Mat channel(img.rows, img.cols, (int)(img.type()&CV_MAT_DEPTH_MASK), data);
		channels.push_back(channel);
		data +=  img.rows*img.cols;
	}
  
	cv::split(img, channels);
	return channels;
}

cv::Mat merge_channels_u(unsigned char* data, int nrows, int ncols)
{
	std::vector<cv::Mat> channels;
	for (int i = 0; i < 3; ++i) {
		cv::Mat channel(nrows, ncols, CV_8UC1, data);
		channels.push_back(channel);
		data +=  nrows*ncols;
	}
	//std::cout<<nrows<<" "<<ncols<<std::endl;
	cv::Mat bgr(nrows, ncols, CV_8UC3);
	cv::merge(channels, bgr);
	cv::Mat rgb(nrows, ncols, CV_8UC3);
	int from_to[] = { 0,2, 1,1, 2,0};
	cv::mixChannels(&bgr, 1, &rgb, 1, from_to, 3);
	return rgb;

}

cv::Mat merge_channels_ua(unsigned char* data, int nrows, int ncols)
{
	std::vector<cv::Mat> channels;
	for (int i = 0; i < 3; ++i) {
		cv::Mat channel(nrows, ncols, CV_8UC1, data);
		channels.push_back(channel);
		data +=  nrows*ncols;
		if (i==2)
			channels.push_back(255-channel);
	}
	// cv::namedWindow( "base", CV_WINDOW_NORMAL );
	// cv::imshow("base",channels[2]);
	// cv::namedWindow( "base2", CV_WINDOW_NORMAL );
	// cv::imshow("base2",255-channels[2]);
	// cv::waitKey(0);	

	cv::Mat bgra(nrows, ncols, CV_8UC4);
	cv::merge(channels, bgra);
	cv::Mat rgba(nrows, ncols, CV_8UC4);
	int from_to[] = { 0,2, 1,1, 2,0, 3,3};
	cv::mixChannels(&bgra, 1, &rgba, 1, from_to, 4);
	return rgba;

}

cv::Mat merge_channels_f(float* data, int nrows, int ncols)
{
	std::vector<cv::Mat> channels;
	for (int i = 0; i < 3; ++i) {
		cv::Mat channel(nrows, ncols, CV_32FC1, data);
		channels.push_back(channel);
		data +=  nrows*ncols;
	}
	//std::cout<<nrows<<" "<<ncols<<std::endl;
	cv::Mat bgr(nrows, ncols, CV_32FC3);
	cv::merge(channels, bgr);
	cv::Mat rgb(nrows, ncols, CV_32FC3);
	int from_to[] = { 0,2, 1,1, 2,0};
	cv::mixChannels(&bgr, 1, &rgb, 1, from_to, 3);
	return rgb;

}


cv::Mat remove_alpha(const cv::Mat& img)
{
	cv::Mat img_tmp(img.size(), img.depth()+8*2);
	int from_to[] = { 0,0, 1,1, 2,2};
	cv::mixChannels(&img, 1, &img_tmp, 1, from_to, 3);
	return img_tmp;
}



cv::Mat repeat_channel(const cv::Mat& img, int channel, int nb)
{
	cv::Mat img_tmp(img.size(), img.depth()+8*(nb-1));
	int *from_to = new int[nb*2];
	for(int i = 0; i < nb; i++)
	{
		from_to[i*2] = channel;
		from_to[i*2+1] = i;
	}
		
	cv::mixChannels(&img, 1, &img_tmp, 1, from_to, nb);
	return  img_tmp;
}


cv::Mat take_alpha(const cv::Mat& img)
{
	cv::Mat img_tmp(img.size(), img.depth()+8*2);
	int from_to[] = { 3,0, 3,1, 3,2};
	cv::mixChannels(&img, 1, &img_tmp, 1, from_to, 3);
	return cv::Scalar::all(255) - img_tmp;
}


cv::Mat inverse_B_R(const cv::Mat& img)
{
	cv::Mat img_tmp(img.size(), img.type());
	if (img.channels() == 3)
	{
		int from_to[] = { 2,0, 1,1, 0,2};
		cv::mixChannels(&img, 1, &img_tmp, 1, from_to, 3);
	}
	else if (img.channels() == 4)
	{
		int from_to[] = { 2,0, 1,1, 0,2,3,3};
		cv::mixChannels(&img, 1, &img_tmp, 1, from_to, 4);
	}
	return img_tmp;
}



cv::Mat flip(const cv::Mat& img)
{
	cv::Mat flipped(img.size(), img.type());
	cv::flip(img, flipped,0);
	return flipped;
}

cv::Mat convert_to_uint(const cv::Mat& img, bool scale)
{
	cv::Mat img_tmp;
	img.convertTo(img_tmp, CV_8U, scale?255:1);
	return img_tmp;
}
cv::Mat convert_to_float(const cv::Mat& img, bool divide)
{
	cv::Mat img_tmp;
	img.convertTo(img_tmp, CV_32F, divide?1/255.0:1);
	return img_tmp;
}

float interpolate( float val, float y0, float x0, float y1, float x1 ) {
    return (val-x0)*(y1-y0)/(x1-x0) + y0;
}

float base( float val ) {
    if ( val <= -0.75 ) return 0;
    else if ( val <= -0.25 ) return interpolate( val, 0.0, -0.75, 1.0, -0.25 );
    else if ( val <= 0.25 ) return 1.0;
    else if ( val <= 0.75 ) return interpolate( val, 1.0, 0.25, 0.0, 0.75 );
    else return 0.0;
}

cv::Vec3b jetColor(float gray)
{
	return cv::Vec3b(base( gray + 0.5 )*255,base( gray )*255, base( gray - 0.5 )*255);
}

cv::Mat dilate_foreground(cv::Mat img, bool(&is_background)(cv::Vec3b))
{
	cv::Mat im2 = img.clone();
	for(int i = 1; i < img.rows-1; i++)
	{
		for(int j = 1; j < img.cols-1; j++)
		{
			if(is_background(img.at<cv::Vec3b>(i,j)))
			{
				//check neighbourhood
				for (int di = -1; di <= 1; di++)
					for (int dj = -1; dj <= 1; dj++)
						if (!is_background(img.at<cv::Vec3b>(i+di,j+dj)))
							im2.at<cv::Vec3b>(i,j)=img.at<cv::Vec3b>(i+di,j+dj);
			}
		}
	}
	return im2;
}

void launch_in_parallel(std::function<void(int,int)> &function, int nthreads, int npoints)
{
    std::vector<std::thread> th;
    int npoints_per_thread = std::ceil((float)npoints/nthreads);
    for (int i = 0; i < nthreads; i++)
        th.push_back(std::thread(function, i,npoints_per_thread));
    for(auto &t : th)
        t.join();
}

template std::vector<cv::Mat> split_channels<unsigned char>(cv::Mat const&, unsigned char*);
template std::vector<cv::Mat> split_channels<float>(cv::Mat const&, float*);
