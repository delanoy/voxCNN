#include <cmath>
#include "rotation_tools.hpp"
#include <Eigen/LU>
#include <iostream>
#include "voxels_tools.hpp"

/********************ROTATION TOOLS***********************/



//Eigen::Vector3f rotate_coords(float x, float y, float z,
//							  const Eigen::Matrix4f &model1,
//							  const Eigen::Matrix4f &model2,
//							  const Eigen::Matrix4f &view1,
//							  const Eigen::Matrix4f &view2,
//							  const Eigen::Matrix4f &proj)
//{
//	//std::cout<<"coords "<<x<<" "<<y<<" "<<z<<std::endl;
//	Eigen::Vector4f NDC(x*2-1, y*2-1, z*2-1, 1.0);
//	Eigen::Vector4f position = (proj*view2*model2).inverse()*NDC;
//	//rotate
//	Eigen::Vector4f coords = proj*view1*model1 * position;
//	coords /= coords.w();
//	//goes back into (0,1)
//	coords = coords.array()/2+0.5;
//	//make sure
//	coords.x() = std::min<float>(0.99,std::max<float>(0.01,coords.x()));
//	coords.y() = std::min<float>(0.99,std::max<float>(0.01,coords.y()));
//	coords.z() = std::min<float>(0.99,std::max<float>(0.01,coords.z()));
//	//std::cout<<"in vox "<<coords.x()<<" "<<coords.y()<<" "<<coords.z()<<std::endl;
//	return coords.head(3);
//}
//
////Eigen::Vector3f rotate_coords(float x, float y, float z,
////							  const Eigen::Matrix4f &model,
////							  const Eigen::Matrix4f &view,
////							  const Eigen::Matrix4f &proj)
////{
////	//std::cout<<"coords "<<x<<" "<<y<<" "<<z<<std::endl;
////	Eigen::Matrix4f PV = proj*view;
////	Eigen::Vector4f NDC(x*2-1, y*2-1, z*2-1, 1.0);
////	Eigen::Vector4f position = model.inverse()*PV.inverse()*NDC;
////	//rotate
////	Eigen::Vector4f coords = PV * position;
////	coords /= coords.w();
////	//goes back into (0,1)
////	coords = coords.array()/2+0.5;
////	//make sure
////	coords.x() = std::min<float>(0.99,std::max<float>(0.01,coords.x()));
////	coords.y() = std::min<float>(0.99,std::max<float>(0.01,coords.y()));
////	coords.z() = std::min<float>(0.99,std::max<float>(0.01,coords.z()));
////	//std::cout<<"in vox "<<coords.x()<<" "<<coords.y()<<" "<<coords.z()<<std::endl;
////	return coords.head(3);
////}
//
//Eigen::Vector3f unproject_vox_coords(float x, float y, float z,
//							  const Eigen::Matrix4f &view,
//							  const Eigen::Matrix4f &proj)
//{
//	// std::cout<<"coords "<<x<<" "<<y<<" "<<z<<std::endl;
//	Eigen::Matrix4f PV = proj*view;
//	float max = 2;
//	Eigen::Vector4f world(x*2*max-max, y*2*max-max, z*2*max-max, 1.0);
//	Eigen::Vector4f position = proj*view*world;
//	//std::cout<<"in vox "<<position[0]/position[3]<<" "<<position[1]/position[3]<<" "<<position[2]/position[3]<<std::endl;
//	position /= position.w();
//	// std::cout<<"NDC "<<NDC[2];
//	// std::cout<<"    in world "<<position[2]<<std::endl;
//	//rotate
//	Eigen::Vector4f coords = position.array()/2+0.5;
//	//coords /= coords.w();
//	//make sure
//	coords.x() = std::min<float>(0.99,std::max<float>(0.01,coords.x()));
//	coords.y() = std::min<float>(0.99,std::max<float>(0.01,coords.y()));
//	coords.z() = std::min<float>(0.99,std::max<float>(0.01,coords.z()));
//	//std::cout<<"in vox "<<coords.x()<<" "<<coords.y()<<" "<<coords.z()<<std::endl;
//	return coords.head(3);
//}
//
//Eigen::Vector3f rotate_vox_coords(float x, float y, float z,
//							  const Eigen::Matrix4f &model,
//							  const Eigen::Matrix4f &view,
//							  const Eigen::Matrix4f &proj)
//{
//	// std::cout<<"coords "<<x<<" "<<y<<" "<<z<<std::endl;
//	Eigen::Matrix4f PV = proj*view;
//	Eigen::Vector4f NDC(x*2-1, y*2-1, z*2-1, 1.0);
//	Eigen::Vector4f position = model.inverse()*PV.inverse()*NDC;
//	// std::cout<<"in world "<<position[0]/position[3]<<" "<<position[1]/position[3]<<" "<<position[2]/position[3]<<std::endl;
//	position /= position.w();
//	// std::cout<<"NDC "<<NDC[2];
//	// std::cout<<"    in world "<<position[2]<<std::endl;
//	//rotate
//	Eigen::Vector4f coords = position;
//	//coords /= coords.w();
//	// //make sure
//	coords.x() = std::min<float>(0.99,std::max<float>(0.01,coords.x()));
//	coords.y() = std::min<float>(0.99,std::max<float>(0.01,coords.y()));
//	coords.z() = std::min<float>(0.99,std::max<float>(0.01,coords.z()));
//	// std::cout<<"in vox "<<coords.x()<<" "<<coords.y()<<" "<<coords.z()<<std::endl;
//	return coords.head(3);
//}

float rotated_voxel_value(const VoxelGrid &vox,
						  const Eigen::Matrix4f &model,
						  const Eigen::Matrix4f &view,
						  const Eigen::Matrix4f &proj,
						  float z,float x, float y, int size_ratio,bool interpolate)
{
	int size = vox.size();
	int size_sample = size*size_ratio;
	//pass coord in the unit cube and rotate it
	Eigen::Vector4f position = from_voxcoords_to_3d(x,y,z,size_sample,true,proj);
	//unproject to world
	position = (proj*view*model).inverse()*position;
	Eigen::Vector3f rotated = from_3d_to_voxcoords(position,size,false,proj,true);

	return get_value(vox,rotated,interpolate);
}


float project_voxel_value(const VoxelGrid &vox,
						  const Eigen::Matrix4f &model,
						  const Eigen::Matrix4f &view,
						  const Eigen::Matrix4f &proj,
						  float z,float x, float y, int size_ratio,bool interpolate)
{
	int size = vox.size();
	int size_sample = size*size_ratio;
	//pass coord in the unit cube and rotate it
	Eigen::Vector4f position = from_voxcoords_to_3d(x,y,z,size_sample,true,proj);
	//unproject to world
	position = (proj*view*model).inverse()*position;
	Eigen::Vector3f rotated = from_3d_to_voxcoords(position,size,false,proj);

	//if(z < 20)
	//std::cout<<" proj = "<<x<<" "<<y<<" "<<z<<", unproj = "<<rotated<<std::endl;
	return get_value(vox,rotated,interpolate);
}



float unproject_voxel_value(const VoxelGrid &vox,
						  const Eigen::Matrix4f &view,
						  const Eigen::Matrix4f &proj,
							float z,float x, float y, int size_ratio,bool interpolate)
{
	int size = vox.size();
	int size_sample = size*size_ratio;
	//pass coord in the unit cube and rotate it
	Eigen::Vector4f position = from_voxcoords_to_3d(x,y,z,size_sample,false,proj);
	//project to screen
	position = proj*view*position;
	Eigen::Vector3f rotated = from_3d_to_voxcoords(position,size,true,proj);

    return get_value(vox,rotated,interpolate);
}


float rotated_proba_value(const VoxelGrid &vox,
						  const Eigen::Matrix4f &model1,
						  const Eigen::Matrix4f &model2,
						  const Eigen::Matrix4f &view1,
						  const Eigen::Matrix4f &view2,
						  const Eigen::Matrix4f &proj,
						  float z,float x, float y, int size_ratio,bool interpolate,
						  int *ni, int *nj,int *nz)
{
	int size = vox.size();
	int size_sample = size*size_ratio;
	//pass coord in the unit cube and rotate it
	Eigen::Vector4f position = from_voxcoords_to_3d(x,y,z,size_sample,true,proj);
	//unproject to world through mat2
	position = (proj*view2*model2).inverse()*position;
	//come back to screen
	position = proj*view1*model1 * position;
	Eigen::Vector3f rotated = from_3d_to_voxcoords(position,size,true,proj);

    return get_value(vox,rotated,interpolate,ni,nj,nz);

}


float rotated_proba_value(const VoxelGrid &vox,
						const Eigen::Matrix4f &model,
						const Eigen::Matrix4f &view,
						const Eigen::Matrix4f &proj,
						  float z,float x, float y, int size_ratio,bool interpolate,
						  int *ni, int *nj,int *nz)
{
	return rotated_proba_value(vox,Eigen::Matrix4f::Identity(),model,view,view,proj,z,x,y,size_ratio,interpolate,ni,nj,nz);

 }


float interpolate(float v1, float v2, float t) {
	// std::cout<<"t = "<<t<<std::endl;
	// std::cout<<"v1 = "<<v1<<", v2 = "<<v2<<" => "<<v1*(1-t) + v2*t<<std::endl;
    return v1*(1-t) + v2*t;
}

float interpolate(const FloatSlice &vox, float x) {
	//one dim of vox is 1
	// std::cout<<"interpolate between "<<x<<" "<<std::endl;
	float v1 = vox(std::floor(x));
	float v2 = vox(std::min<int>(std::ceil(x),vox.cols()-1) );
	float t = x-std::floor(x);
    return interpolate(v1,v2,t);;
}

float interpolate_bilinear(const FloatSlice &vox, float x, float y)
{
	// std::cout<<"interpolate between "<<x<<" "<<y<<" "<<std::endl;
	//interpolate 1 side
	float v1 = interpolate(vox.row(std::floor(x)),y);
	//interpolate 2nd size
	float v2 = interpolate(vox.row(std::min<int>(std::ceil(x),vox.rows()-1)),y);
	//interpolate between the 2
	return interpolate(v1,v2,x-std::floor(x));
}
float interpolate_trilinear(const VoxelGrid &vox, float x, float y, float z)
{
	// std::cout<<"interpolate between "<<x<<" "<<y<<" "<<z<<" "<<std::endl;
	//interpolate 1 slice
	float v1 = interpolate_bilinear(vox.slice(std::floor(z)),x,y);
	//interpolate 2nd slice
	float v2 = interpolate_bilinear(vox.slice(std::min<int>(std::ceil(z),vox.size()-1)),x,y);
	//interpolate between the 2
	return interpolate(v1,v2,z-std::floor(z));
}

float z_to_depth(int z, int size, const Eigen::Matrix4f &proj)
{
	// std::cout<<"z : "<<z;
	// std::cout<<"=> "<<std::round(z*0.8+8)<<"("<<z*0.8+8<<")";
	float zf=z*0.8+(size/16);
	float depth = -(zf)/size*5.5-2.5;
	// std::cout<<"=> "<<depth;
	depth = -proj(2,3)/depth-proj(2,2);
	// std::cout<<"=> "<<depth;
	depth=depth/2+0.5;
	// std::cout<<"=> "<<depth<<std::endl;
	return depth;
}

float depth_to_z(float depth, int size, const Eigen::Matrix4f &proj)
{
	// std::cout<<"depth : "<<depth;
	depth = depth * 2 - 1;
	// std::cout<<"=> "<<depth;
	depth = -proj(2,3)/(depth+proj(2,2));
	// std::cout<<"=> "<<depth;
	float z = (-(depth+2.5)/5.5)*size;
	// std::cout<<"=> "<<z;
	// std::cout<<"=> "<<std::round((z-8)/0.8-0.01)<<"("<<(z-8)/0.8<<")"<<std::endl;
	return (z-(size/16))/0.8;
}


Eigen::Vector4f from_voxcoords_to_3d(int x, int y, int z, int size_sample, bool to_screen,const Eigen::Matrix4f &proj)
{
	Eigen::Vector3f coords_01((float)(y)/(size_sample), 1-(float)(x)/(size_sample),1-(float)(z)/(size_sample));
	if (to_screen)
		coords_01.z() = z_to_depth(z, size_sample, proj);
	float max = to_screen? 1 : 2;
	Eigen::Vector4f position(coords_01.x()*2*max-max, coords_01.y()*2*max-max, coords_01.z()*2*max-max, 1.0);
	return position;
}

Eigen::Vector3f from_3d_to_voxcoords(Eigen::Vector4f& position, int size, bool to_screen, const Eigen::Matrix4f &proj, bool normalized )
{
	position /= position.w();
	float max = to_screen? 1 : 2;
	Eigen::Vector4f coords = (position.array()+max)/(2*max);
	if(normalized)
		coords = position;
	//make sure
	coords.x() = std::min<float>(0.99,std::max<float>(0.01,coords.x()));
	coords.y() = std::min<float>(0.99,std::max<float>(0.01,coords.y()));
	coords.z() = std::min<float>(0.99,std::max<float>(0.01,coords.z()));

	Eigen::Vector3f voxcoords((1-coords.y())*(size), coords.x()*(size), (1-coords.z())*(size));

	if (to_screen) {
		voxcoords.z() = depth_to_z(coords.z(), size, proj);
		voxcoords.z() = std::min<float>(size - 1,std::max<float>(0,voxcoords.z()));
	}

	return voxcoords;
}




float get_value(const VoxelGrid &vox, const Eigen::Vector3f& rotated, bool interpolate, int *ni, int *nj,int *nz)
{
    int size = vox.size();
    if (!interpolate)
	{
		int new_i = (int)std::floor(rotated.x());
		int new_j = (int)std::floor(rotated.y());
		int new_z = (int)std::round(rotated.z());
        if(ni!=NULL)
		{
			*ni = new_i;
			*nj = new_j;
			*nz = new_z;
		}
		float v0 = vox(new_i, new_j,new_z);
		return v0;
	} else
	{
		return interpolate_trilinear(vox,rotated.x(), rotated.y(),rotated.z());
	}
}