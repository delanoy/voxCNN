//
// Created by delanoy on 12/11/18.
//

#include "convolution_kernel.hpp"

Eigen::RowVectorXf gaussian_kernel_1d(int dim)
{
	Eigen::RowVectorXf ker = Eigen::RowVectorXf(dim);
	if (dim == 5)
		ker << 0.061, 0.242, 0.383, 0.242, 0.061;
	else if (dim == 3)
		ker << 0.228814,	0.542373,	0.228814;
	return ker;
}
FloatSlice gaussian_kernel_2d(int dim, Eigen::RowVectorXf *kernel_1d)
{
	FloatSlice ker  = FloatSlice(dim, dim);
	//build 2d kernel
	Eigen::RowVectorXf g1D;
	if (kernel_1d == NULL)
		g1D = gaussian_kernel_1d(dim);
	else
		g1D = *kernel_1d;
	int i;
	for (i = 0; i <= dim/2; i++)
		ker.row(i) = g1D * g1D(dim-1-i);
	for ( ; i<dim; i++)
		ker.row(i) = ker.row(dim-1-i);
	return ker;
}

VoxelGrid gaussian_kernel_3d(int dim, Eigen::RowVectorXf *kernel_1d)
{
	VoxelGrid ker(dim);

	//build 2d kernel
	Eigen::RowVectorXf g1D;
	if (kernel_1d == NULL)
		g1D = gaussian_kernel_1d(dim);
	else
		g1D = *kernel_1d;
	ker.slice(dim/2) = gaussian_kernel_2d(dim, kernel_1d);
	//build 3D kernel
	int i;
	for (i = 0; i <= dim/2; i++)
		ker.slice(i) = ker.slice(dim/2) * g1D(dim-1-i);
	for ( ; i<dim; i++)
		ker.slice(i) = ker.slice(dim-1-i);

	return ker;
}
