//
// Created by delanoy on 5/23/19.
//

#ifndef VOXCNN_NORMAL_FUSION_HPP
#define VOXCNN_NORMAL_FUSION_HPP


#include <DGtal/base/Common.h>
#include <DGtal/helpers/StdDefs.h>
#include <DGtal/helpers/Shortcuts.h>
#include <DGtal/helpers/ShortcutsGeometry.h>
//#include <QTS>


using namespace DGtal;
using namespace Z3i;


typedef Shortcuts<Z3i::KSpace> SH3;
typedef ShortcutsGeometry<Z3i::KSpace> SHG3;

#include "tools/voxels_tools.hpp"




CountedPtr<SH3::BinaryImage> GenerateBinaryImageFromVoxelGrid(const VoxelGrid & vox, float threshold);


SH3::RealVectors populate_normals_from_gradient(const VoxelGrid & vox, const SH3::SurfelRange & surfels,
        const CanonicSCellEmbedder<Z3i::KSpace> &  embedder);

//SH3::RealVectors all_normals_from_gradient(const VoxelGrid & vox)
//{
//    Domain domain(Point(0,0,0),Point(vox.size()-1,vox.size()-1,vox.size()-1));
//    std::vector<VoxelGrid> gradient = vox.compute_gradient();
//    //std::cout<<domain.size()<<std::endl;
//    SH3::RealVectors surf_normals(domain.size());
//    int i = 0;
//    for(auto &p: domain) {
//        Eigen::Vector3f norm_ijk(
//                gradient[0](ijk_to_xyz(p[0],p[1],p[2],vox.size())),
//                gradient[1](ijk_to_xyz(p[0],p[1],p[2],vox.size())),
//                gradient[2](ijk_to_xyz(p[0],p[1],p[2],vox.size())));
//        surf_normals[i] = RealVector(norm_ijk[1],-norm_ijk[0],-norm_ijk[2]);
//        i++;
//    }
//    return surf_normals;
//}

//project a point in 3D space p to an image (integer pixel coordinates + depth)
void project_realpoint_to_image(const RealPoint & p,
                                const Eigen::Matrix4f &model,
                                const Eigen::Matrix4f &view,
                                const Eigen::Matrix4f &proj,
                                int & img_i, int & img_j, float & depth);

//unproject a normal from image space (normal map) to 3d world
RealVector unproject_normal_to_surfel(const cv::Vec3b &normal_img,
                                      const Eigen::Matrix4f &model,
                                      const Eigen::Matrix4f &view,
                                      const Eigen::Matrix4f &proj);



SH3::RealVectors project_normal_map_onto_surfels(const cv::Mat &normals,
                                                 const VoxelGrid & unpacked,
                                                 const Eigen::Matrix4f &model,
                                                 const Eigen::Matrix4f &view_orig,
                                                 const Eigen::Matrix4f &view,
                                                 const Eigen::Matrix4f &proj,
                                                 const SH3::SurfelRange & surfels,
                                                 const CanonicSCellEmbedder<Z3i::KSpace> &  embedder, const double noise_amount=0);



////copy the mean of per view normals into estimated ones
//SH3::RealVectors copy_perview_normals_into_global_ones(const SH3::RealVectors &ii_normals, const std::vector<SH3::RealVectors> &normals_v)
//{
//    SH3::RealVectors normals_copy = ii_normals;
//    for (int i = 0; i < ii_normals.size(); ++i) {
//
//        int found_normal = 0;
//        RealVector mean_norm(0,0,0);
//        for (int idv = 0; idv < normals_v.size(); ++idv) {
//            if(normals_v[idv][i].norm() > 0) {
//                mean_norm += normals_v[idv][i];
//                found_normal ++;
//            }
//        }
//        if (found_normal > 0) {
//            normals_copy[i] = mean_norm/found_normal;
//        }
//    }
//    return normals_copy;
//}



//compute mean of all normals (per view + estimated) and confidence (As variance)
void compute_mean_and_confidence(const SH3::RealVectors &ii_normals, const std::vector<SH3::RealVectors> &normals_v,SH3::RealVectors &out_normals, std::vector<float> &confidence);


void regularize_surface(const SH3::KSpace & K,
                        const CountedPtr< SH3::DigitalSurface > &surface,
                        const SH3::SurfelRange & surfels,
                        const SH3::RealVectors &ii_normals,
                        std::vector<Z3i::RealPoint> &originalPositions,
                        std::vector<Z3i::RealPoint> &regularizedPositions);



SH3::RealVectors  get_AT_normals(const SH3::SurfelRange &surfels,
                                 const SH3::RealVectors &ii_normals,
								 const double alpha_at  = 0.1);

SH3::RealVectors  get_weighted_AT_normals(const SH3::SurfelRange &surfels,
                                          const SH3::RealVectors &ii_normals,
                                          const SH3::Scalars     &confidence );


float confidence_from_variance(float var);


#endif //VOXCNN_NORMAL_FUSION_HPP
