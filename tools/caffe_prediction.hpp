#ifndef _CAFFE_PREDICTION
#define _CAFFE_PREDICTION

#include <caffe/caffe.hpp>
#include "voxels_tools.hpp"

std::vector<VoxelGrid> predict(const std::string& model_file,
								const std::string& trained_file,const cv::Mat& img);
VoxelGrid predict_volumetric_voxels(caffe::Net<float> &caffe_net,
								const VoxelGrid& grid);
VoxelGrid predict_voxels(caffe::Net<float> &caffe_net,const cv::Mat& img,
						 const VoxelGrid& pred);
VoxelGrid predict_voxels(caffe::Net<float> &net, const cv::Mat& img);
cv::Mat predict_normals(caffe::Net<float> &caffe_net, const cv::Mat& img,
						const VoxelGrid& pred);
cv::Mat predict_normals(caffe::Net<float> &caffe_net, const cv::Mat& img);

VoxelGrid predict_voxels_update(caffe::Net<float> &caffe_simple, caffe::Net<float> &caffe_update,
                                int nb_it, int nb_views,
                                std::vector<cv::Mat> &im,
                                std::vector< Eigen::Matrix4f > &model,
                                std::vector< Eigen::Matrix4f > &view,
                                const Eigen::Matrix4f &proj);

#endif
