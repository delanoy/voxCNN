//
// Created by delanoy on 5/23/19.
//


#include "tools/image_tools.hpp"
#include "AT/ATSolver-u2-v0.h"

#include "normal_fusion.hpp"

using namespace std;

Eigen::Vector3i ijk_to_xyz(int x, int y, int z, int size)
{
    //return Eigen::Vector3i(x,y,z);
    return Eigen::Vector3i(size-y-1,x,size-z-1);
}
Eigen::Vector3i ijk_to_xyz(const Eigen::Vector3i & p, int size)
{
    return ijk_to_xyz(Eigen::Vector3i(p[0],p[1],p[2]),size);
}

//return true is depth is in front of ref_depth
bool is_visible(float depth, float ref_depth, float epsilon)
{
    //return true;
    if(ref_depth == 0 || ref_depth == 1)
        return true;
    if(depth-epsilon > ref_depth)
        return false;
    return true;
}


float confidence_from_variance(float var) {
    float conf= 2*var * var*var * var;
    //if (conf > 0.8) conf = 5;
    return conf;
//    float sigma = 0.6;
//    return 5*exp(-(var*var)/(sigma*sigma));
}




CountedPtr<SH3::BinaryImage> GenerateBinaryImageFromVoxelGrid(const VoxelGrid &vox, float threshold) {
    Domain domain(Point(0,0,0),Point(vox.size()-1,vox.size()-1,vox.size()-1));
    //Point center(64,64,64);
    SH3::BinaryImage *image = new SH3::BinaryImage(domain);

    //Then you can iterate over the domain points
    // p[i] gives you access to the point coordinates
    for(auto &p: domain) {
        if (vox(ijk_to_xyz(p[0],p[1],p[2],vox.size())) > threshold)
            //if (vox(p[0],p[1],p[2]) > threshold)
            image->setValue(p, true);
        else
            image->setValue(p, false);
    }
    return CountedPtr<SH3::BinaryImage>(image);
}

SH3::RealVectors populate_normals_from_gradient(const VoxelGrid &vox, const SH3::SurfelRange &surfels,
                                                const CanonicSCellEmbedder<Z3i::KSpace> &embedder) {
    int nsurfels = surfels.size();
    std::vector<VoxelGrid> gradient = vox.compute_gradient();
    SH3::RealVectors surf_normals(nsurfels);
    for(auto i=0; i < surfels.size(); i++) {
        RealPoint p = embedder(surfels[i]);
        Eigen::Vector3f norm_ijk(
                gradient[0](ijk_to_xyz(p[0],p[1],p[2],vox.size())),
                gradient[1](ijk_to_xyz(p[0],p[1],p[2],vox.size())),
                gradient[2](ijk_to_xyz(p[0],p[1],p[2],vox.size())));
        surf_normals[i] = RealVector(norm_ijk[1],-norm_ijk[0],-norm_ijk[2]);
    }
    return surf_normals;
}

void project_realpoint_to_image(const RealPoint &p, const Eigen::Matrix4f &model, const Eigen::Matrix4f &view,
                                const Eigen::Matrix4f &proj, int &img_i, int &img_j, float &depth) {
    //translate to world coord
    Eigen::Vector3f coords_01(p[0], p[1], p[2]);
    float max = 2;
    Eigen::Vector4f position(coords_01.x() * 2 * max - max, coords_01.y() * 2 * max - max,
                             coords_01.z() * 2 * max - max, 1.0);
    //project and send to [0,1]
    Eigen::Vector4f projected = proj * view * model * position;
    projected /= projected.w();
    Eigen::Vector4f coords = (projected.array() + 1) / (2);

    //make sure
    coords.x() = std::min<float>(0.99, std::max<float>(0.01, coords.x()));
    coords.y() = std::min<float>(0.99, std::max<float>(0.01, coords.y()));
    coords.z() = std::min<float>(0.99, std::max<float>(0.01, coords.z()));
    img_i = (1 - coords.y()) * (256);
    img_j = (coords.x()) * (256);
    depth = coords.z();
}

RealVector
unproject_normal_to_surfel(const cv::Vec3b &normal_img, const Eigen::Matrix4f &model, const Eigen::Matrix4f &view,
                           const Eigen::Matrix4f &proj) {
    //go to [0,1] space (+ rectify BGR opencCV)
    Eigen::Vector3f norm(normal_img[2], normal_img[1], normal_img[0]);
    norm /= 255;
    norm = norm.array() * 2 - 1;
//unproject
    Eigen::Vector4f norm_3d(norm[0], norm[1], norm[2], 0); //TODO already in xyz space
    Eigen::Vector4f unproj_norm = (view * model).inverse() * norm_3d;
    return RealVector(unproj_norm[0], unproj_norm[1], unproj_norm[2]);
}

SH3::RealVectors
project_normal_map_onto_surfels(const cv::Mat &normals, const VoxelGrid &unpacked, const Eigen::Matrix4f &model,
                                const Eigen::Matrix4f &view_orig, const Eigen::Matrix4f &view,
                                const Eigen::Matrix4f &proj, const SH3::SurfelRange &surfels,
                                const CanonicSCellEmbedder<Z3i::KSpace> &embedder, const double noise_amount) {
    trace.beginBlock("project normal map onto surfels");
    //prepare data
    VoxelGrid proj_grid = unpacked.rotate_voxels_prediction(Eigen::Matrix4f::Identity(),model,view_orig,view,proj,4,true);//.blur();
    cv::Mat depth_vox = proj_grid.iso_surface(0.5, false);
    //cv::imshow("iso surface", proj_grid.iso_surface(0.5, false));
//    cv::waitKey();
    SH3::RealVectors proj_normals(surfels.size());
    std::cout<<proj_grid.size()<<std::endl;
    std::default_random_engine generator;
    std::normal_distribution<double> distribution(0.0,noise_amount);


    //project the normal on each surfel
    std::function<void(int,int)> project_one_normal = [&](int p, int npoints){
        //std::cout<<p<<" doing\n";
        //std::cout<<npoints<<" doing\n";
        for (int i_vox = npoints*p; i_vox < std::min<int>(npoints*(p+1),surfels.size()); ++i_vox)
            //for (int i_vox = 0; i_vox < surfels.size(); ++i_vox)
        {
            //std::cout<<i_vox<<" doing\n";
            RealPoint p = embedder(surfels[i_vox]) / unpacked.size();
            int img_i, img_j;
            float depth;
            project_realpoint_to_image(p,model, view, proj, img_i, img_j, depth);
			if(noise_amount>0){
				img_i += distribution(generator);
				img_j += distribution(generator);
			}

            if(is_visible(depth,depth_vox.at<float>(img_i, img_j),1.0/64)) {
                proj_normals[i_vox] = unproject_normal_to_surfel(normals.at<cv::Vec3b>(img_i, img_j), model, view, proj);
                if(proj_normals[i_vox].norm()<0.1)
                {
                    proj_normals[i_vox] = RealVector(0,0,0);
                }

            }
            else
                proj_normals[i_vox] = RealVector(0,0,0);
        }

    };
    launch_in_parallel(project_one_normal,10,surfels.size());
    trace.endBlock();
    return proj_normals;
}

void compute_mean_and_confidence(const SH3::RealVectors &ii_normals, const std::vector<SH3::RealVectors> &normals_v,
                                 SH3::RealVectors &out_normals, std::vector<float> &confidence) {
    bool use_orig_normals = true;
    out_normals.resize(ii_normals.size());
    confidence.resize(ii_normals.size());
    for (int i = 0; i < ii_normals.size(); ++i) {
        int found_normal = use_orig_normals? 1 : 0;
        //compute mean
        RealVector mean_norm = use_orig_normals? ii_normals[i] : RealVector(0,0,0);
        for (int idv = 0; idv < normals_v.size(); ++idv) {
            if(normals_v[idv][i].norm() > 0) {
                mean_norm += normals_v[idv][i];
                found_normal ++;
            }
        }
        if (found_normal > 0)
            mean_norm /= found_normal;
        else
            mean_norm = ii_normals[i];
        //compute variance (using dot product as difference with mean)
        float variance = 0;
        if (use_orig_normals)
            variance = (ii_normals[i].cosineSimilarity(mean_norm))*(ii_normals[i].cosineSimilarity(mean_norm));
        for (int idv = 0; idv < normals_v.size(); ++idv) {
            if(normals_v[idv][i].norm() > 0) {
                variance += (normals_v[idv][i].cosineSimilarity(mean_norm))*(normals_v[idv][i].cosineSimilarity(mean_norm));
            }
        }
        if (found_normal > 0)
            variance /= found_normal;


        out_normals[i] = mean_norm;
        confidence[i] = std::max<float>(0,1-std::sqrt(variance));
        if(found_normal == (use_orig_normals? 1 : 0)) //no reprojection, only II normals
            confidence[i] = 0.8;
        //std::cout<<confidence[i]<<std::endl;
    }
}







//////////////////////////////METHODS FOR DIGITAL SURFACES

void regularize_surface(const SH3::KSpace &K, const CountedPtr<SH3::DigitalSurface> &surface,
                        const SH3::SurfelRange &surfels, const SH3::RealVectors &ii_normals,
                        std::vector<Z3i::RealPoint> &originalPositions,
                        std::vector<Z3i::RealPoint> &regularizedPositions) {
    auto embedderCell   = SH3::getCellEmbedder( K );
    SH3::Cell2Index  cellIndex;
    auto pointels        = SH3::getPointelRange(cellIndex,surface);
    SH3::Surfel2Index surfelIndex;
    auto polySurf        = SH3::makeDualPolygonalSurface(surfelIndex,surface);
    auto faces = polySurf->allFaces() ;
    auto dsurf_faces = surface->allClosedFaces(); // Faces of digital surface (umbrellas)
    // dsurf_pointels list the pointels in the same order as the faces of polySurf.
    std::vector<SH3::Cell> dsurf_pointels( dsurf_faces.size() );
    std::transform( dsurf_faces.cbegin(), dsurf_faces.cend(),
                    dsurf_pointels.begin(),
                    [&] ( const SH3::DigitalSurface::Face f ) { return K.unsigns(surface->pivot( f )); } );


    // Original points
    originalPositions.clear();
    std::transform(pointels.begin(), pointels.end(), std::back_inserter(originalPositions),
                   [embedderCell](SH3::Cell &pointel ) {return embedderCell(pointel);  } );

    // Regularized points (copied)
    regularizedPositions = originalPositions;

    // Energy gradient
    std::vector<RealVector> gradient(originalPositions.size(), RealVector(0,0,0));
    std::vector<RealVector> gradientAlign(originalPositions.size(), RealVector(0,0,0));
    std::vector<unsigned char> numberAdjEdgesToPointel(originalPositions.size(),0);

    // Precompute all relations for align energy
    std::vector< SH3::Idx > align_pointels_idx( surfels.size() * 4 );
    for(int i = 0; i < surfels.size(); ++i)
    {
        auto pointelsAround = surface->facesAroundVertex(surfels[i], true);
        ASSERT(pointelsAround.size() == 4);
        for(auto j = 0; j < 4; ++j)
        {
            auto p      = K.unsigns(surface->pivot(pointelsAround[ j ]));
            auto cell_p = cellIndex[ p ];
            align_pointels_idx[ 4*i + j ] = cell_p;
            numberAdjEdgesToPointel[ cell_p ] ++;
        }
    }

    // Precompute all relations for fairness energy
    std::vector< SH3::Idx > fairness_pointels_idx;
    std::vector< unsigned int > nbAdjacent( faces.size() );
    for(int faceId=0 ; faceId < faces.size(); ++faceId)
    {
        auto           idx = cellIndex[ dsurf_pointels[ faceId ] ];
        fairness_pointels_idx.push_back( idx );
        unsigned int nbAdj = 0;
        auto          arcs = polySurf->arcsAroundFace(faceId);
        for(auto anArc : arcs)
        {
            // /!\ we must check that the opposite exists (aka surface with boundaries)
            auto      op = polySurf->opposite(anArc);
            auto adjFace = polySurf->faceAroundArc(op);
            ASSERT(adjFace != faceId);
            fairness_pointels_idx.push_back( cellIndex[ dsurf_pointels[ adjFace] ] );
            nbAdj++;
        }
        ASSERT(nbAdj>0);
        nbAdjacent[ faceId ] = nbAdj;
    }

    ////////////:
    //Energy
    std::vector<double> alpha(originalPositions.size(), 0.001); //Per vertex data-attachment
    const double beta  = 1.0;   //align
    const double gamma = .1;    //fairness
    bool first_iter    = true;
    double last_energy = 0.0;
    double dt          = 1.0;

    const double two_beta  = 2.0 * beta;
    const double two_gamma = 2.0 * gamma;

    trace.beginBlock("Gradient");
    for(auto step = 0; step < 260; ++step)
    {
        double energy= 0.0;

        //data attach
        for(int i = 0; i < originalPositions.size(); ++i)
        {
            auto delta_d     = originalPositions[i] - regularizedPositions[i];
            energy          += alpha[i] * delta_d.squaredNorm() ;
            gradient[i]      = 2.0*alpha[i] * delta_d;
            //reset for align
            gradientAlign[i] = RealVector(0,0,0);
        }

        //align
        auto it = align_pointels_idx.cbegin();
        for(int i = 0; i < surfels.size(); ++i)
        {
            auto cell_p0 = *it++;
            auto cell_p1 = *it++;
            auto cell_p2 = *it++;
            auto cell_p3 = *it++;
            auto e0 = regularizedPositions[ cell_p0 ] - regularizedPositions[ cell_p1 ];
            auto e1 = regularizedPositions[ cell_p1 ] - regularizedPositions[ cell_p2 ];
            auto e2 = regularizedPositions[ cell_p2 ] - regularizedPositions[ cell_p3 ];
            auto e3 = regularizedPositions[ cell_p3 ] - regularizedPositions[ cell_p0 ];
            auto cos_a0 = e0.dot( ii_normals[i] );
            auto cos_a1 = e1.dot( ii_normals[i] );
            auto cos_a2 = e2.dot( ii_normals[i] );
            auto cos_a3 = e3.dot( ii_normals[i] );
            energy += beta * ( cos_a0 * cos_a0 + cos_a1 * cos_a1
                               + cos_a2 * cos_a2 + cos_a3 * cos_a3 );
            gradientAlign[ cell_p0 ] += cos_a0 * ii_normals[i];
            gradientAlign[ cell_p1 ] += cos_a1 * ii_normals[i];
            gradientAlign[ cell_p2 ] += cos_a2 * ii_normals[i];
            gradientAlign[ cell_p3 ] += cos_a3 * ii_normals[i];
        }
        for(auto i=0; i < originalPositions.size(); ++i)
        {
            ASSERT(numberAdjEdgesToPointel[i] >0);
            gradient[i] += two_beta * gradientAlign[i] / (double)numberAdjEdgesToPointel[i];
        }



        //fairness
        auto itP  = fairness_pointels_idx.cbegin();
        auto itNb = nbAdjacent.cbegin();
        for(int faceId=0 ; faceId < faces.size(); ++faceId)
        {
            auto           idx = *itP++;
            unsigned int nbAdj = *itNb++;
            Z3i::RealPoint barycenter(0.0, 0.0, 0.0);
            RealPoint     phat = regularizedPositions[ idx ];
            for ( unsigned int i = 0; i < nbAdj; ++i )
                barycenter += regularizedPositions[ *itP++ ];
            ASSERT(nbAdj>0);
            barycenter      /= (double)nbAdj;
            auto delta_f     = phat - barycenter;
            energy          += gamma * delta_f.squaredNorm() ;
            gradient[ idx ] += two_gamma * delta_f;
        }

        double gradnorm=0.0;
        for(auto &v: gradient)
            gradnorm = std::max(gradnorm, v.norm());

        trace.info()<< "Step " << step
                    << " dt=" << dt
                    << " energy = " << energy
                    << "  gradnorm= " << gradnorm << std::endl;
        // JOL: slight modif to converge 5 times faster.
        if ( ! first_iter )
            dt *= ( energy > last_energy ) ? 0.5 : 1.1;
        if ( dt < 0.0001 ) break; // no need to continue
        last_energy = energy;
        first_iter  = false;
        //One step advection
        for(int i=0; i < regularizedPositions.size(); ++i)
            regularizedPositions[i] -= dt * gradient[i];
    }
    trace.endBlock();

}

SH3::RealVectors get_AT_normals(const SH3::SurfelRange &surfels, const SH3::RealVectors &ii_normals, const double alpha_at) {
//DEC init
    //const double alpha_at  = 0.1;
    const double lambda_at = 0.1;
    const double e1        = 4.0;
    const double e2        = 0.5;
    const double er        = 2.0;
    typedef DiscreteExteriorCalculus<2,3, EigenLinearAlgebraBackend>   Calculus;
    typedef DiscreteExteriorCalculusFactory<EigenLinearAlgebraBackend> CalculusFactory;

    const Calculus calculus = CalculusFactory::createFromNSCells<2>( surfels.begin(), surfels.end() );
    ATSolver< KSpace > at_solver(calculus);

    //Remaping normals  (FIXME: update init instead of duplicating)
    std::map<SCell,RealVector> n_estimations;
    std::map<SCell,int> cell_ids;
    for(auto i=0; i < surfels.size(); ++i) {
        n_estimations[surfels[i]] = ii_normals[i];
        cell_ids[surfels[i]] = i;

    }

    at_solver.init(n_estimations);
    at_solver.setUp( alpha_at, lambda_at );

    //-----------------------------------------------------------------------------
    // Solving AT functional.
    trace.beginBlock( "Solving AT functional. " );
    const int n = 5;
    double min_eps = e1;
    ATSolver<KSpace>::SmallestEpsilonMap smallest_eps;
    for ( double eps = e1; eps >= e2; eps /= er )
    {
        trace.info() << "************** epsilon = " << eps << "***************************************" << endl;
        at_solver.setEpsilon( eps );
        min_eps = eps;
        for ( int i = 0; i < n; ++i )
        {
            trace.info() << "-------------------------------------------------------------------------------" << endl;
            trace.info() << "---------- Iteration " << i << "/" << n << " ------------------------------" << endl;
            at_solver.solveU2V0( true );

            trace.beginBlock("Checking v, computing norms");
            at_solver.checkV0();
            double n_infty = 0.0;
            double n_2 = 0.0;
            double n_1 = 0.0;
            at_solver.diffV0( n_infty, n_2, n_1 );
            trace.info() << "Variation |v^k+1 - v^k|_oo = " << n_infty << std::endl;
            trace.info() << "Variation |v^k+1 - v^k|_2 = " << n_2 << std::endl;
            trace.info() << "Variation |v^k+1 - v^k|_1 = " << n_1 << std::endl;
            trace.endBlock();
            if ( n_infty < 1e-4 ) break;
        }
        at_solver.updateSmallestEpsilonMap(smallest_eps);
    }
    trace.endBlock();

    //u is the reconstructed normal vector field
    //(array of 3 Primal 2-form)
    const auto u = at_solver.u2;
    //v0 is the dected feature
    // (Primal 0-form)
    const auto v0 = at_solver.v0;
    SH3::RealVectors at_normals(surfels.size());
    for (int index = 0; index < surfels.size(); ++index) {
        const SCell&      cell = u[ 0 ].getSCell( index );
        int surf_id = cell_ids[cell];
        at_normals[surf_id][0] = u[ 0 ].myContainer( index );
        at_normals[surf_id][1] = u[ 1 ].myContainer( index );
        at_normals[surf_id][2] = u[ 2 ].myContainer( index );
    }
    return at_normals;
}

SH3::RealVectors get_weighted_AT_normals(const SH3::SurfelRange &surfels, const SH3::RealVectors &ii_normals,
                                         const SH3::Scalars &confidence) {
//DEC init
    // const double alpha_at  = 0.2;
    // const double lambda_at = 0.05;
    // const double e1        = 3.0;
    const double alpha_at  = 0.1;
    const double lambda_at = 0.05;
    const double e1        = 4.0;
    const double e2        = 0.5;
    const double er        = 2.0;
    typedef DiscreteExteriorCalculus<2,3, EigenLinearAlgebraBackend>   Calculus;
    typedef DiscreteExteriorCalculusFactory<EigenLinearAlgebraBackend> CalculusFactory;
    typedef typename Calculus::PrimalForm2                 PrimalForm2;
    typedef typename RealVector::Component Scalar;

    const Calculus calculus = CalculusFactory::createFromNSCells<2>( surfels.begin(), surfels.end() );
    ATSolver< KSpace > at_solver(calculus);

    //Remaping normals  (FIXME: update init instead of duplicating)
    std::map<SCell,RealVector> n_estimations;
    std::map<SCell,Scalar>     w_estimations;
    std::map<SCell,int> cell_ids;
    for(auto i=0; i < surfels.size(); ++i) {
        n_estimations[surfels[i]] = ii_normals[i];
        w_estimations[surfels[i]] = confidence[i];
        cell_ids[surfels[i]] = i;
    }
    at_solver.init(n_estimations);
    at_solver.setUpWithWeightedAlpha( alpha_at, lambda_at, w_estimations );

    //-----------------------------------------------------------------------------
    // Solving AT functional.
    trace.beginBlock( "Solving AT functional with weighted alpha. " );
    const int n = 5;
    double min_eps = e1;
    ATSolver<KSpace>::SmallestEpsilonMap smallest_eps;
    for ( double eps = e1; eps >= e2; eps /= er )
    {
        trace.info() << "************** epsilon = " << eps << "***************************************" << endl;
        at_solver.setEpsilon( eps );
        min_eps = eps;
        for ( int i = 0; i < n; ++i )
        {
            trace.info() << "-------------------------------------------------------------------------------" << endl;
            trace.info() << "---------- Iteration " << i << "/" << n << " ------------------------------" << endl;
            at_solver.solveU2V0( true );

            trace.beginBlock("Checking v, computing norms");
            at_solver.checkV0();
            double n_infty = 0.0;
            double n_2 = 0.0;
            double n_1 = 0.0;
            at_solver.diffV0( n_infty, n_2, n_1 );
            trace.info() << "Variation |v^k+1 - v^k|_oo = " << n_infty << std::endl;
            trace.info() << "Variation |v^k+1 - v^k|_2 = " << n_2 << std::endl;
            trace.info() << "Variation |v^k+1 - v^k|_1 = " << n_1 << std::endl;
            trace.endBlock();
            if ( n_infty < 1e-4 ) break;
        }
        at_solver.updateSmallestEpsilonMap(smallest_eps);
    }
    trace.endBlock();

    //u is the reconstructed normal vector field
    //(array of 3 Primal 2-form)
    const auto u = at_solver.u2;
    //v0 is the dected feature
    // (Primal 0-form)
    const auto v0 = at_solver.v0;
    SH3::RealVectors at_normals(surfels.size());
    for (int index = 0; index < surfels.size(); ++index) {
        const SCell&      cell = u[ 0 ].getSCell( index );
        int surf_id = cell_ids[cell];
        at_normals[surf_id][0] = u[ 0 ].myContainer( index );
        at_normals[surf_id][1] = u[ 1 ].myContainer( index );
        at_normals[surf_id][2] = u[ 2 ].myContainer( index );
    }
    return at_normals;
}



