#ifndef _PREDICTION_TOOLS
#define _PREDICTION_TOOLS

#include <Eigen/Core>
#include <opencv2/core.hpp>
#include <vector>
//#include "read_voxels.hpp"


typedef Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic, Eigen::RowMajor> FloatSlice;
typedef std::vector<FloatSlice> FloatGrid;


class VoxelGrid
{
private:
    FloatGrid content;



public:

    VoxelGrid()
    {
        content.resize(0);
    }

    VoxelGrid(int dim)
    {
        content.resize(dim);
        for(FloatSlice & slice : content)
            slice = FloatSlice(dim,dim);
    }

    VoxelGrid(int dim, int val)
    {
        content.resize(dim);
        for(FloatSlice & slice : content)
            slice = FloatSlice::Constant(dim,dim,val);
    }

    VoxelGrid operator+(const VoxelGrid& v);

    float operator()(int i, int j, int z) const
    {
        return content[z](i,j);
    }
    float & operator()(int i, int j, int z)
    {
        return content[z](i,j);
    }

    float operator()(const Eigen::Vector3i & coords) const
    {
        return (*this)(coords[0],coords[1],coords[2]);
    }
    float& operator()(const Eigen::Vector3i & coords)
    {
        return (*this)(coords[0],coords[1],coords[2]);
    }

    size_t size() const
    {
        return content.size();
    }

    FloatSlice &slice(int c)
    {
        return content[c];
    }

    const FloatSlice &slice(int c) const
    {
        return content[c];
    }


    int coord(int i) const
    {
        if (i >= size()) return size() -1;
        else if (i<0) return 0;
        else return i;
    }



    VoxelGrid binarize(float threshold) const;

    cv::Mat pack_in_image(int grid_rows, int grid_cols) ;
    static VoxelGrid unpack_from_image( cv::Mat &image, int grid_rows, int grid_cols);
    cv::Mat pred_to_grid(int grid_rows, int grid_cols, int begin=0);


    void gaussian_noise(float intensity);


    VoxelGrid rotate_voxels(
            const Eigen::Matrix4f &model,
            const Eigen::Matrix4f &view,
            const Eigen::Matrix4f &proj, int size_ratio = 1,bool interpolate=false) const ;

    VoxelGrid rotate_voxels_prediction(const Eigen::Matrix4f &model1,
                                       const Eigen::Matrix4f &model2,
                                       const Eigen::Matrix4f &view1,
                                       const Eigen::Matrix4f &view2,
                                       const Eigen::Matrix4f &proj, int size_ratio =1,bool interpolate=false) const;
    VoxelGrid rotate_voxels_prediction(const Eigen::Matrix4f &model1,
                                       const Eigen::Matrix4f &view1,
                                       const Eigen::Matrix4f &proj, int size_ratio =1,bool interpolate=false) const;
    VoxelGrid unproject_voxels_prediction(const Eigen::Matrix4f &view,
                                          const Eigen::Matrix4f &proj, int size_ratio = 1,bool interpolate=false) const;
    VoxelGrid
    project_voxels_prediction(const Eigen::Matrix4f &model, const Eigen::Matrix4f &view, const Eigen::Matrix4f &proj,
                              int size_ratio=1, bool interpolate=false) const;

    VoxelGrid blur() const;

    VoxelGrid convolution_3d(const VoxelGrid &kernel) const;
    std::vector<VoxelGrid> compute_gradient() const;

    cv::Mat iso_surface(float threshold= 0.5, bool colorize = true) const;
    cv::Mat normal_map(float threshold, bool blur = false) const;
    cv::Mat normal_map(float threshold,
                       std::vector<VoxelGrid> &norm) const;



};



#endif
