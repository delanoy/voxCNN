#ifndef _ROTATION_TOOLS
#define _ROTATION_TOOLS
#include <Eigen/Core>
#include <vector>
//typedef Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic, Eigen::RowMajor> CaffeMap;
//typedef cv::Mat CaffeMap;
//typedef std::vector<CaffeMap> Prediction;

class VoxelGrid;

typedef Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic, Eigen::RowMajor> FloatSlice;
typedef std::vector<FloatSlice> FloatGrid;


/********************ROTATION TOOLS***********************/

//rotate ground truth voxel grid : lies in [0,1]
float rotated_voxel_value(const VoxelGrid &vox,
						  const Eigen::Matrix4f &model,
						  const Eigen::Matrix4f &view,
						  const Eigen::Matrix4f &proj,
						  float x, float y, float z, int size_ratio = 1,bool interpolate=false);
//int c, int i, int j);
float rotated_proba_value(const VoxelGrid &vox,
						const Eigen::Matrix4f &model,
						const Eigen::Matrix4f &view,
						const Eigen::Matrix4f &proj,
						  float z,float x, float y, int size_ratio = 1,bool interpolate=false,
						  int *ni=NULL, int *nj=NULL,int *nz=NULL);
float rotated_proba_value(const VoxelGrid &vox,
						  const Eigen::Matrix4f &model1,
						  const Eigen::Matrix4f &model2,
						  const Eigen::Matrix4f &view1,
						  const Eigen::Matrix4f &view2,
						  const Eigen::Matrix4f &proj,
						  float z,float x, float y, int size_ratio = 1,bool interpolate=false,
						  int *ni=NULL, int *nj=NULL,int *nz=NULL);
float unproject_voxel_value(const VoxelGrid &vox,
						  const Eigen::Matrix4f &view,
						  const Eigen::Matrix4f &proj,
							float z,float x, float y, int size_ratio = 1,bool interpolate=false);
float project_voxel_value(const VoxelGrid &vox,
						 const Eigen::Matrix4f &model,
						  const Eigen::Matrix4f &view,
						  const Eigen::Matrix4f &proj,
							float z,float x, float y, int size_ratio = 1,bool interpolate=false);

//Eigen::Vector3f rotate_coords(float x, float y, float z,
//							  const Eigen::Matrix4f &model,
//							  const Eigen::Matrix4f &view,
//							  const Eigen::Matrix4f &proj);
//Eigen::Vector3f rotate_coords(float x, float y, float z,
//							  const Eigen::Matrix4f &model1,
//							  const Eigen::Matrix4f &model2,
//							  const Eigen::Matrix4f &view1,
//							  const Eigen::Matrix4f &view2,
//							  const Eigen::Matrix4f &proj);
//
//Eigen::Vector3f rotate_vox_coords(float x, float y, float z,
//                                  const Eigen::Matrix4f &model,
//                                  const Eigen::Matrix4f &view,
//								  const Eigen::Matrix4f &proj);
//Eigen::Vector3f unproject_vox_coords(float x, float y, float z,
//                                     const Eigen::Matrix4f &view,
//                                     const Eigen::Matrix4f &proj);




float get_value(const VoxelGrid &vox, const Eigen::Vector3f& rotated, bool interpolate,int *ni=NULL, int *nj=NULL,int *nz=NULL);
Eigen::Vector4f from_voxcoords_to_3d(int x, int y, int z, int size_sample, bool to_screen,const Eigen::Matrix4f &proj);
Eigen::Vector3f from_3d_to_voxcoords(Eigen::Vector4f& position, int size, bool to_screen, const Eigen::Matrix4f &proj, bool normalized = false );

float z_to_depth(int z, int size, const Eigen::Matrix4f &proj);
float depth_to_z(float depth, int size, const Eigen::Matrix4f &proj);

float interpolate(const FloatSlice &vox, float x);
float interpolate_bilinear(const FloatSlice &vox, float x, float y);
float interpolate_trilinear(const VoxelGrid &vox, float x, float y, float z);

#endif
