#include "caffe_prediction.hpp"
#include <tools/image_tools.hpp>

using namespace caffe;  // NOLINT(build/namespaces)

#include <highgui.h>
#include <chrono>
#include <opencv2/highgui.hpp>


bool send_grid_to_net(Net<float> &caffe_net, int id_in, const VoxelGrid& pred)
{
    int size = pred.size();
	Blob<float>* input_pred_blob = caffe_net.input_blobs()[id_in];
	if(input_pred_blob->count() != size*size*size)
	{
		std::cout<<"Prediction is not at the right size\n";
		return false;
	}

	float * input_pred = input_pred_blob->mutable_cpu_data();
	for(int c = 0; c < size; c++)
	{
		std::memcpy(input_pred, pred.slice(c).data() ,size * size * sizeof(float));
		input_pred += size*size;
	}
    return true;
}
bool send_image_to_net(Net<float> &caffe_net, int id_in, const cv::Mat & img)
{
    Blob<float>* input_image = caffe_net.input_blobs()[id_in];
	if(input_image->count() != img.total()*img.channels())
	{
		std::cout<<"Image is not at the right size\n";
		return false;
	}

	cv::Mat img_float = convert_to_float(img, false);
	std::vector<cv::Mat> input_channels = split_channels(img_float, input_image->mutable_cpu_data());

	CHECK(reinterpret_cast<float*>(input_channels.at(0).data)
		  == input_image->cpu_data())
		<< "Input channels are not wrapping the input layer of the network.";
    return true;
}
VoxelGrid get_grid_from_net(Net<float> &caffe_net, int id_out)
{
	Blob<float>* pred_blob = caffe_net.output_blobs()[id_out];
	int output_width = pred_blob->width();
	int output_height = pred_blob->height();
	int size = output_width;

	VoxelGrid grid(size);
	const float* output_data=pred_blob->cpu_data();
	output_data+=output_width * output_height;
	for(int c = 0; c < size; c++)
	{
		std::memcpy(grid.slice(c).data(), output_data, size*size* sizeof(float));
		output_data += size*size;
	}
	return grid;
}
cv::Mat get_image_from_net(Net<float> &caffe_net, int id_out)
{
	Blob<float>* output = caffe_net.output_blobs()[id_out];
	int output_width = output->width();
	int output_height = output->height();

	cv::Mat img = merge_channels_f(output->mutable_cpu_data(), output_height, output_width);
	return img;

}

VoxelGrid predict_voxels(Net<float> &caffe_net,const cv::Mat& img,
								const VoxelGrid& pred)
{
	using namespace std::chrono;

	//send sketch
    send_image_to_net(caffe_net,0,img);

	//send previous prediction
    send_grid_to_net(caffe_net,1,pred);


	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	caffe_net.Forward();
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	//std::cout<<"Duration of prediction: "<<duration_cast<milliseconds>(t2-t1).count()<<std::endl;

	VoxelGrid grid = get_grid_from_net(caffe_net,0);

	// imShow::show(img,"sketch");
	// cv::Mat vox_rgb2=iso_surface(grid, 0.2);
	// imShow::show(vox_rgb2,"pred");
	// imShow::show(normal_map(grid,0.2),"norm");
	// imShow::wait();

	
	// cv::namedWindow( "sketch", CV_WINDOW_NORMAL );
	// cv::imshow("sketch",img);
	// // cv::Mat vox_cv = pred_to_grid(grid,16,size/16);
	// // cv::namedWindow( "vox", CV_WINDOW_NORMAL );
	// // cv::imshow("vox",vox_cv);

	// cv::namedWindow( "pred", CV_WINDOW_NORMAL );
	// cv::moveWindow("pred",0,600);
	// cv::imshow("pred",vox_rgb2);
	// cv::waitKey(0);

	return grid;
}


VoxelGrid predict_volumetric_voxels(Net<float> &caffe_net,
								const VoxelGrid& grid)
{
	using namespace std::chrono;

	//send previous prediction
    send_grid_to_net(caffe_net,0,grid);

	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	caffe_net.Forward();
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	//std::cout<<"Duration of prediction: "<<duration_cast<milliseconds>(t2-t1).count()<<std::endl;

	VoxelGrid res = get_grid_from_net(caffe_net,0);

	return res;
}



VoxelGrid predict_voxels(Net<float> &caffe_net, const cv::Mat& img)
{
	using namespace std::chrono;
//send sketch
    send_image_to_net(caffe_net,0,img);

	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	caffe_net.Forward();
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	//std::cout<<"Duration of prediction: "<<duration_cast<milliseconds>(t2-t1).count()<<std::endl;

	VoxelGrid grid = get_grid_from_net(caffe_net,0);

	//std::cout<<"Prediction copied\n";

	// cv::namedWindow( "sketch", CV_WINDOW_NORMAL );
	// cv::imshow("sketch",img);
	// // cv::Mat vox_cv = pred_to_grid(grid,16,size/16);
	// // cv::namedWindow( "vox", CV_WINDOW_NORMAL );
	// // cv::imshow("vox",vox_cv);

	// cv::Mat vox_rgb2=iso_surface(grid, 0.5);
	// cv::namedWindow( "pred", CV_WINDOW_NORMAL );
	// cv::moveWindow("pred",0,600);
	// cv::imshow("pred",vox_rgb2);
	// cv::waitKey(0);
	return grid;
	
}


cv::Mat predict_normals(Net<float> &caffe_net, const cv::Mat& img,
								const VoxelGrid& pred)
{
	using namespace std::chrono;


	//send sketch
    send_image_to_net(caffe_net,0,img);

	//send previous prediction
    send_grid_to_net(caffe_net,1,pred);

	std::cout<<"Data copied\n";
	
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	caffe_net.Forward();
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	std::cout<<"Duration of prediction: "<<duration_cast<milliseconds>(t2-t1).count()<<std::endl;
	std::cout<<"Prediction made\n";

	cv::Mat normals = get_image_from_net(caffe_net, 0);

	return normals;
}

cv::Mat predict_normals(Net<float> &caffe_net, const cv::Mat& img)
{
	using namespace std::chrono;

	//send sketch
    send_image_to_net(caffe_net,0,img);

	std::cout<<"Data copied\n";

	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	caffe_net.Forward();
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	std::cout<<"Duration of prediction: "<<duration_cast<milliseconds>(t2-t1).count()<<std::endl;
	std::cout<<"Prediction made\n";

	cv::Mat normals = get_image_from_net(caffe_net, 0);

	return normals;
}

VoxelGrid predict_voxels_update(Net<float> &caffe_simple, Net<float> &caffe_update,
                                int nb_it, int nb_views,
                                std::vector<cv::Mat> &im,
                                std::vector< Eigen::Matrix4f > &model,
                                std::vector< Eigen::Matrix4f > &view,
                                const Eigen::Matrix4f &proj)
{
    int i1 = 0;
    int i2 = (i1 + 1) % nb_views;

    VoxelGrid old_pred = predict_voxels(caffe_simple, im[i1]);
    VoxelGrid new_pred;
    for(int it = 1; it < nb_it; it ++)
    {
        for (int v = 0; v < nb_views; v++)
        {
            //std::cout<<"pred from view "<<i2<<" using pred from "<<i1<<std::endl;
            //bring pred from v1 to v2

//			cv::imshow("before",old_pred.pred_to_grid(8, old_pred.size()/16.0, 16));
//			cv::imshow("before rot",old_pred.normal_map( 0.4));
//			cv::imshow("before rot iso",old_pred.iso_surface( 0.4));
//            cv::waitKey();

//			std::cout<<model[i1]<<std::endl<<std::endl;
//			std::cout<<view[i1]<<std::endl<<std::endl;
//			std::cout<<model[i2]<<std::endl<<std::endl;
//			std::cout<<view[i2]<<std::endl<<std::endl;
//			std::cout<<proj<<std::endl<<std::endl;

            old_pred = old_pred.rotate_voxels_prediction( model[i1], model[i2],
                                                view[i1], view[i2], proj,1,true);

//            cv::imshow("after rot",old_pred.normal_map(0.4));
//            cv::imshow( "after",old_pred.pred_to_grid(8, old_pred.size()/16.0, 16));
//			cv::imshow("after rot iso",old_pred.iso_surface( 0.4));
//            cv::waitKey();

            //launch net
            new_pred = predict_voxels(caffe_update, im[i2], old_pred);

            i1 = i2;
            i2 = (i1 + 1) % nb_views;

            old_pred=new_pred;
        }
    }
	return old_pred;
}
