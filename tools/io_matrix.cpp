#include "io_matrix.h"
#include <fstream>
#include <iostream>
#include <vector>

template <typename DerivedM>
bool loadMatrix(std::string filename, Eigen::PlainObjectBase<DerivedM>& M)
{
	// General structure
	// 1. Read file contents into vector<double> and count number of lines
	// 2. Initialize matrix
	// 3. Put data in vector<double> into matrix
 
	std::ifstream input(filename.c_str());
	if (input.fail())
	{
		std::cerr << "ERROR. Cannot find file '" << filename << "'." << std::endl;
		M.resize(0,0);
		return false;
	}
	std::string line;
	typename DerivedM::Scalar val;
 
	std::vector<typename DerivedM::Scalar> v;
	int n_rows = 0;
	while (getline(input, line))
	{
		++n_rows;
		std::stringstream input_line(line);
		while (!input_line.eof())
		{
			input_line >> val;
			v.push_back(val);
		}
	}
	input.close();
 
	int n_cols = v.size()/n_rows;
	M.resize(n_rows,n_cols);
	for (int i=0; i<n_rows; i++)
		for (int j=0; j<n_cols; j++)
			M(i,j) = v[i*n_cols + j];

 
	return true;
}

 
template <typename DerivedM>
bool saveMatrix(std::string filename,  const Eigen::PlainObjectBase<DerivedM> &M)
{
	std::ofstream file;
	file.open(filename.c_str());
	if (!file.is_open())
	{
		std::cerr << "Couldn't open file '" << filename << "' for writing." << std::endl;
		return false;
	}
 
	file << std::fixed;
	file << M;
	file.close();
 
	return true;
 
}

template bool loadMatrix<Eigen::Matrix<float, -1, -1, 0, -1, -1> >(std::string, Eigen::PlainObjectBase<Eigen::Matrix<float, -1, -1, 0, -1, -1> >&);
template bool loadMatrix<Eigen::Matrix<int, -1, -1, 0, -1, -1> >(std::string, Eigen::PlainObjectBase<Eigen::Matrix<int, -1, -1, 0, -1, -1> >&);
