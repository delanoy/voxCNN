#ifndef IO_MATRIX_H
#define IO_MATRIX_H

#ifndef IGL_NO_EIGEN
#include <Eigen/Core>
#endif

#include <string>
template <typename DerivedM>
bool loadMatrix(std::string filename, Eigen::PlainObjectBase<DerivedM>& M);

template <typename DerivedM>
bool saveMatrix(std::string filename,  const Eigen::PlainObjectBase<DerivedM>& M);

//#include "io_matrix.cpp"

#endif
