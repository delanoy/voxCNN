//
// Created by delanoy on 12/11/18.
//

#ifndef VOXCNN_CONVOLUTION_KERNEL_HPP
#define VOXCNN_CONVOLUTION_KERNEL_HPP


#include "voxels_tools.hpp"

VoxelGrid gaussian_kernel_3d(int dim=5, Eigen::RowVectorXf * kernel_1d=0);
FloatSlice gaussian_kernel_2d(int dim=5, Eigen::RowVectorXf * kernel_1d=0);
Eigen::RowVectorXf gaussian_kernel_1d(int dim=5);



#endif //VOXCNN_CONVOLUTION_KERNEL_HPP
