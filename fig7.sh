#first line (left to right) : No noise
#fig7/armchair_2views_regularizedSurf_mean.obj
#fig7/armchair_2views_regularizedSurf_AT.obj
#fig7/armchair_2views_regularizedSurf_WAT.obj

#2nd line (left to right) : global shift
#fig7/armchair_2views_global_regularizedSurf_mean.obj
#fig7/armchair_2views_global_regularizedSurf_AT.obj
#fig7/armchair_2views_global_regularizedSurf_WAT.obj

#3rd line (left to right) : local shift
#fig7/armchair_2views_local_regularizedSurf_mean.obj
#fig7/armchair_2views_local_regularizedSurf_AT.obj
#fig7/armchair_2views_local_regularizedSurf_WAT.obj


#get normal maps and volumetric prediction
./cnn_prediction.sh test_data/armchair_2views

mkdir fig7/
####no noise
./build/regularize_normal_noise test_data/armchair_2views fig7/armchair_2views 0
####global shift
#replace normal map with shifted version
convert test_data/armchair_2views_1_normals.png -repage -5-5  -background "rgb(125,125,125)" -flatten test_data/armchair_2views_1_normals_shifted.png 
cp test_data/armchair_2views_1_normals{,_orig}.png
cp test_data/armchair_2views_1_normals{_shifted,}.png
./build/regularize_normal_noise test_data/armchair_2views fig7/armchair_2views_global 0
#get back original map
cp test_data/armchair_2views_1_normals{_orig,}.png
####local shift
./build/regularize_normal_noise test_data/armchair_2views fig7/armchair_2views_local 5
