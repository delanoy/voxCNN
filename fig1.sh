#fig1 a) : test_data/armchair_2views_X_clean.png
#fig1 b) : test_data/armchair_2views_X_normals.png and fig1_2_3/armchair_2views_initialSurf.obj
#fig1 c) : fig1_2_3/armchair_2views_normals_all.obj
#fig1 d) : fig1_2_3/armchair_2views_normals_mean_confidence.obj
#fig1 e) : fig1_2_3/armchair_2views_normals_final.obj
#fig1 f) : fig1_2_3/armchair_2views_regularizedSurf_WAT.obj

#get normal maps and volumetric prediction
./cnn_prediction.sh test_data/armchair_2views

mkdir fig1_2_3/
./build/regularize_all_outputs test_data/armchair_2views fig1_2_3/armchair_2views
