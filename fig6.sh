#first line (left to right) : No noise
#fig6/armchair_2views_0_initialSurf.obj
#fig6/armchair_2views_0_regularizedSurf_grad.obj
#fig6/armchair_2views_0_regularizedSurf_WAT.obj

#2nd line (left to right) : noise=0.1
#fig6/armchair_2views_0_1_initialSurf.obj
#fig6/armchair_2views_0_1_regularizedSurf_grad.obj
#fig6/armchair_2views_0_1_regularizedSurf_WAT.obj

#3rd line (left to right) : noise=0.2
#fig6/armchair_2views_0_2_initialSurf.obj
#fig6/armchair_2views_0_2_regularizedSurf_grad.obj
#fig6/armchair_2views_0_2_regularizedSurf_WAT.obj


#get normal maps and volumetric prediction
./cnn_prediction.sh test_data/armchair_2views

mkdir fig6/
./build/regularize_volumetric_noise test_data/armchair_2views fig6/armchair_2views_0 0
./build/regularize_volumetric_noise test_data/armchair_2views fig6/armchair_2views_0_1 0.1
./build/regularize_volumetric_noise test_data/armchair_2views fig6/armchair_2views_0_2 0.2

