sudo apt install cmake
sudo apt install libopencv-dev libeigen3-dev libgflags-dev libgoogle-glog-dev libprotobuf-dev libboost-dev libboost-system-dev libatlas-base-dev
sudo apt install caffe-cpu libcaffe-cpu-dev
sudo apt install nvidia-cuda-toolkit #libcudart9.1 libcublas9.1
sudo apt install imagemagick

git clone https://github.com/DGtal-team/DGtal.git
cd DGtal; mkdir build; cd build
cmake .. -DWITH_EIGEN=true
sudo make install
cd ..

