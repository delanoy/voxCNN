path_in=$1

if [ -e $1"_pred.png" ]
then
    echo "Predictions already done"
    exit
fi

./build/predictions \
    networks/deploy_pix2pix_64.prototxt networks/snapshot_pix2pix_64_iter_760000.caffemodel \
    networks/deploy_pix2pix_pair_64.prototxt networks/snapshot_pix2pix_pair_64_iter_650000.caffemodel \
    networks/deploy_pix2pix_normal_only.prototxt networks/solver_pix2pix_normal_only_iter_1500000.caffemodel \
    $1 $1
