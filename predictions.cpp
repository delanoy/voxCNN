#include "tools/io_matrix.h" 


#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include <fstream>
#include <vector>
#include  <ctime>

#include <Eigen/Core>
#include <Eigen/LU>

#include <tools/voxels_tools.hpp>
#include <tools/caffe_prediction.hpp>
//#include <tools/rotation_tools.hpp>
//#include <tools/convolution_tools.hpp>
//#include <tools/dataset_tools.hpp>
#include "tools/image_tools.hpp"
//#include <tools/transformation_tools.hpp>
//#include <igl/mat_to_quat.h>

#include <opencv2/imgproc.hpp>



#define mod(a,b) ((a)<0?(a)+(b):(a)%(b))

std::string i2str(int a)
{
	std::ostringstream ostr;
	ostr<<a;
	return ostr.str();
}

std::string path_out;



#include <chrono>
using namespace std::chrono;

int main(int argc, char *argv[])
{
	if (argc < 8)
	{
		std::cout<<"usage : "<<argv[0]<<" deploy1 snapshot1 deploy_pair snapshot_pair deploy_normals snapshot_normals path path_out\n";
        std::cout<<"Take output of a drawing session in path and run it through networks. Save mesh and packed prediction.";
		return 1;
	}
	FLAGS_minloglevel = 1;

	std::string deploy1 = argv[1];
	std::string snapshot1 = argv[2];
	std::string deploy_pair = argv[3];
	std::string snapshot_pair = argv[4];
	std::string deploy_normals = argv[5];
	std::string snapshot_normals = argv[6];

	std::string path = argv[7];
	path_out = argv[8];
	
	caffe::Caffe::set_mode(caffe::Caffe::CPU);
	caffe::Net<float> net1(deploy1, caffe::TEST);
	net1.CopyTrainedLayersFrom(snapshot1);
	caffe::Net<float> net_pair(deploy_pair, caffe::TEST);
	net_pair.CopyTrainedLayersFrom(snapshot_pair);
	caffe::Net<float> net_normals(deploy_normals, caffe::TEST);
	net_normals.CopyTrainedLayersFrom(snapshot_normals);

	std::cout<<path<<std::endl<<std::endl;

	//load viewpoints
	std::vector< Eigen::Matrix4f>view;
	std::vector< Eigen::Matrix4f>mv;
	Eigen::Matrix4f proj;

	Eigen::MatrixXf views;
	loadMatrix(path+".txt", views);
	int nb_views = views.rows() / 4;
	std::cout<<"Reading sketches from "<<nb_views<<" views"<<std::endl<<std::endl;
	for (int v = 0; v < nb_views; v++)
	{
		view.push_back(views.block<4,4>(v*4,4));
		mv.push_back(views.block<4,4>(v*4,0));
	}
	proj = views.block<4,4>(0,8);


	//read sketches
	std::vector<cv::Mat> im;
	unsigned char* img_data;
	for (int v = 0; v < nb_views; v++)
	{
		im.push_back(cv::imread(path+"_"+std::to_string(v)+"_clean.png"));
		if ( im[v].rows == 0)
			std::cout<<"can't find image "<<path<<"_"<<std::to_string(v)<<".png"<<std::endl;
	}

	Eigen::MatrixXd V;
	Eigen::MatrixXi F;
		

	VoxelGrid pred = predict_voxels_update(net1,net_pair,3,nb_views,im, mv, view, proj);
    //VoxelGrid pred = predict_voxels(net1,im[0]);
	int size = pred.size();


    VoxelGrid pred_can1 = pred.rotate_voxels_prediction(mv[0].inverse().eval(), view[0], proj, 1, true);

//	marching_cube(pred_can1,view[0],proj,V,F,1);
//	igl::writeOBJ(path_out+".obj", V, F);
//

	 int nrows = 256/size;
	 int ncols = size/4/nrows;
	 cv::imwrite(path_out+"_pred.png", pred_can1.pack_in_image(nrows, ncols));
	
	for (int v = 0; v < nb_views; v++) {
		std::vector<cv::Mat> im_channels = split_channels<float>(im[v]);
		cv::Mat normals = predict_normals(net_normals, im_channels[0]);
        cv::imwrite(path_out+"_"+std::to_string(v)+"_normals.png", inverse_B_R(convert_to_uint(normals,true)));
	}
}
