#include <iostream>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <DGtal/base/Common.h>
#include <DGtal/helpers/StdDefs.h>
#include <DGtal/helpers/Shortcuts.h>
#include <DGtal/helpers/ShortcutsGeometry.h>

#include <normal_fusion.hpp>
//#define WITH_VISU3D_QGLVIEWER


#include <DGtal/io/boards/Board3D.h>

#include <Eigen/LU>

#include "tools/voxels_tools.hpp"
#include "tools/io_matrix.h"
#include "tools/image_tools.hpp"

int main(int argc, char**argv)
{
    std::string path_in = argv[1];
    int dim = std::stoi(argv[2]);
    int nviews;

    auto params = SH3::defaultParameters() | SHG3::defaultParameters();
    // Set your own parameters with operator().
    params("gridstep", 1.0 )("r-radius","3.0");

    trace.beginBlock("loading data");
    //load viewpoints
    Eigen::MatrixXf viewpoints;
    loadMatrix(path_in+".txt", viewpoints);
    nviews = viewpoints.rows()/4;
    Eigen::Matrix4f view, proj;
    view = viewpoints.block<4,4>(0,4);
    proj = viewpoints.block<4,4>(0,8);
    Eigen::Matrix4f model_id = Eigen::Matrix4f::Identity();

    // load image of the prediction
    int nrows = 256/dim;
    int ncols = dim/4/nrows;
    cv::Mat packed = cv::imread(path_in+"_pred.png",-1);
    if(packed.cols > dim*ncols)
        packed = packed(cv::Rect(256,0,dim*ncols,256));
    VoxelGrid unpacked = VoxelGrid::unpack_from_image(packed,nrows, ncols);
    //unpacked.gaussian_noise(0.2);
    //unproject voxel grid
    VoxelGrid grid = unpacked.unproject_voxels_prediction(view,proj,1,true);

    //generate DGtal object
    auto shape = GenerateBinaryImageFromVoxelGrid(grid,0.5);

    trace.endBlock();

    trace.beginBlock("create topology and normals");
    //Topology
    auto K          = SH3::getKSpace( shape );
    auto embedder   = SH3::getSCellEmbedder( K );
    auto surface    = SH3::makeDigitalSurface( shape, K, params );
    auto surfels    = SH3::getSurfelRange( surface, params );

    //Geometry
    //use normals from gradient or probabilities
    auto ii_normals = populate_normals_from_gradient(grid,surfels,embedder);
    trace.info()<<"We have "<<surfels.size() <<" surfels."<<std::endl;
    trace.endBlock();

    ///////treat each view (if not orthogonal)
    trace.beginBlock("project per view normal maps");
    std::vector<SH3::RealVectors> normals_v(nviews);
    int n_ortho_views = 0; //count the number of orthogonal views
    for (int idv = 0; idv < nviews; ++idv) {
        trace.info()<<"Treating view "<<idv<<std::endl;

        Eigen::Matrix4f model = viewpoints.block<4, 4>(idv * 4, 0);
        //verifier si c'est bien une vue perspective
        if(model.block<3,3>(0,0).maxCoeff() == 1 || model.block<3,3>(0,0).minCoeff() == -1)
        {
            n_ortho_views ++;
            trace.info()<<"Ignoring view "<<idv<<std::endl;
            continue;
        }

        cv::Mat normals = cv::imread(path_in + "_" + std::to_string(idv) + "_clean-outputs.png", 3);
        cv::Mat sketch = cv::imread(path_in + "_" + std::to_string(idv) + "_clean.png", -1);


        normals_v[idv-n_ortho_views] = project_normal_map_onto_surfels(normals, unpacked, model, view, viewpoints.block<4, 4>(idv * 4, 4), proj, surfels, embedder);

//        //cv::imshow("packed", grid.pred_to_grid(8, 8));
//        cv::imshow("normal", superimpose_sketch(normals, sketch));
//        VoxelGrid proj_grid = unpacked.rotate_voxels_prediction(Eigen::Matrix4f::Identity(),model,view,viewpoints.block<4, 4>(idv * 4, 4),proj,4,true);//.blur();
//        cv::Mat depth_vox = proj_grid.normal_map(0.5, false);
//        cv::imshow("iso surface", superimpose_sketch(depth_vox, sketch));
//        //cv::imshow("grid", proj_grid.pred_to_grid(8, 8));
//        cv::waitKey();
    }
    nviews -= n_ortho_views;
    normals_v.resize(nviews);

    trace.info()<<"Compute mean and confidence "<<std::endl;
    SH3::RealVectors mean;
    std::vector<float> confidence;
    compute_mean_and_confidence(ii_normals,normals_v,mean,confidence);
    trace.endBlock();

        //list of colors use in viewer
    std::vector<Color> colors;
    colors.emplace_back(Color::Red);
    colors.emplace_back(Color::Blue);
    colors.emplace_back(Color::Green);
    colors.emplace_back(Color(255,255,0));
    colors.emplace_back(Color(255,0,255));
    colors.emplace_back(Color(0,255,255));


//////////////AT/WAT
     SHG3::Scalars sq_confidence( confidence.size() );
     for ( unsigned int i = 0; i < sq_confidence.size(); ++i ) {
         sq_confidence[i] = confidence_from_variance(confidence[i]);
     }
    auto wat_normals   = get_weighted_AT_normals( surfels, mean, sq_confidence );
  //   auto at_normals = get_AT_normals(surfels, mean);
  //   auto at_normals_orig = get_AT_normals(surfels, ii_normals);


   //// Exporting all different normals
     {
         Board3D<Space, KSpace> board(K);
         board << SetMode3D(surfels[0].className(), "Basic");
         for (auto i = 0; i < surfels.size(); ++i) {
             board << CustomColors3D(Color::White, Color::White);
             board << surfels[i];
             RealPoint centroid = embedder(surfels[i]);
             for (int idv = 0; idv < nviews; ++idv) {
                 if(normals_v[idv][i].norm() > 0) {
                     board << CustomColors3D(colors[idv+1],colors[idv+1]);
                     board.addCylinder(centroid, centroid + normals_v[idv][i], 0.08);
                 }
             }
             RealVector n = ii_normals[i];
             board << CustomColors3D(colors[0],colors[0]);
             board.addCylinder(centroid, centroid + n, 0.08);
         }
         board.saveOBJ(path_in+"_normals_all.obj");
     }
   ////



   //// Exporting candidate normal n_bar with confidence as color
     {
         Board3D<Space, KSpace> board(K);
         board << SetMode3D(surfels[0].className(), "Basic");
         for (auto i = 0; i < surfels.size(); ++i) {
             board << CustomColors3D(Color::White, Color::White);
             board << surfels[i];
             RealPoint centroid = embedder(surfels[i]);
             float score = confidence_from_variance(confidence[i]);
             Color conf_color = Color(255*(1-score),255*(score),0);
             board << CustomColors3D(conf_color,conf_color);
             board.addCylinder(centroid, centroid + mean[i], 0.08);
         }
         board.saveOBJ(path_in+"_normals_mean_confidence.obj");
     }
   ////


  // //// Exporting piecewise smooth normals (WAT)
  //   {
  //       Board3D<Space, KSpace> board(K);
  //       board << SetMode3D(surfels[0].className(), "Basic");
  //       for (auto i = 0; i < surfels.size(); ++i) {
  //           board << CustomColors3D(Color::White, Color::White);
  //           board << surfels[i];
  //           RealPoint centroid = embedder(surfels[i]);
  //           RealVector n = wat_normals[i];
  //           board << CustomColors3D(Color::Red, Color::Red);
  //           board.addCylinder(centroid, centroid + n, 0.08);
  //       }
  //       board.saveOBJ(path_in+"_normals_final.obj");
  //   }
  // ////
  

//////////////// REGULARIZATION
    std::vector<Z3i::RealPoint> originalPositions,regularizedPositions;
    SH3::Cell2Index  cellIndex;
    SH3::getPointelRange(cellIndex,surface);


//    //try each view separately
//    for (int idv = 0; idv < nviews; ++idv) {
//        auto perview_norm = copy_oneview_normals_into_global_ones(ii_normals,normals_v[idv]);
//        regularize_surface(K,surface,surfels,perview_norm,originalPositions,regularizedPositions);
//        SH3::saveOBJ(surface, [&] (const SH3::Cell &c){ return regularizedPositions[ cellIndex[c]];},
//                     perview_norm, SH3::Colors(), path_in+"_regularizedSurf"+std::to_string(idv)+".obj");
//    }


    // //surface from initial normals
    // regularize_surface(K,surface,surfels,ii_normals,originalPositions,regularizedPositions);
    // SH3::saveOBJ(surface, [&] (const SH3::Cell &c){ return regularizedPositions[ cellIndex[c]];},
    //                  ii_normals,
    //              SH3::Colors(), path_in+"_regularizedSurf_grad.obj");

    //     //surface from AT on intial normals
    // regularize_surface(K,surface,surfels,at_normals_orig,originalPositions,regularizedPositions);
    // at_normals_orig.clear(); //remove normals to avoid errors
    // SH3::saveOBJ(surface, [&] (const SH3::Cell &c){ return regularizedPositions[ cellIndex[c]];},
    //                  at_normals_orig,
    //              SH3::Colors(), path_in+"_regularizedSurf_grad_AT.obj");

    // //surface from  mean normals
    // regularize_surface(K,surface,surfels,mean,originalPositions,regularizedPositions);
    // mean.clear(); //remove normals to avoid errors
    // SH3::saveOBJ(surface, [&] (const SH3::Cell &c){ return regularizedPositions[ cellIndex[c]];},
    //                  mean,
    //              SH3::Colors(), path_in+"_regularizedSurf_mean.obj");


    // //surface from AT on mean normals
    // regularize_surface(K,surface,surfels,at_normals,originalPositions,regularizedPositions);
    // at_normals.clear(); //remove normals to avoid errors
    // SH3::saveOBJ(surface, [&] (const SH3::Cell &c){ return regularizedPositions[ cellIndex[c]];},
    //                  at_normals,
    //              SH3::Colors(), path_in+"_regularizedSurf_AT.obj");


    //surface from weighted AT
    regularize_surface(K,surface,surfels,wat_normals,originalPositions,regularizedPositions);
    wat_normals.clear(); //remove normals to avoid errors
    SH3::saveOBJ(surface, [&] (const SH3::Cell &c){ return regularizedPositions[ cellIndex[c]];},
                       wat_normals,
                 SH3::Colors(), path_in+"_regularizedSurf_WAT.obj");



    return 0;
}

