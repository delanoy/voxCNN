#pragma once
#ifndef _ATSOLVER_U2_V0_H_
#define _ATSOLVER_U2_V0_H_

#include <iostream>
#include "DGtal/base/Common.h"
#include "DGtal/kernel/domains/HyperRectDomain.h"
#include "DGtal/topology/CubicalComplex.h"
// DEC
#include "DGtal/math/linalg/EigenSupport.h"
#include "DGtal/dec/DiscreteExteriorCalculus.h"
#include "DGtal/dec/DiscreteExteriorCalculusSolver.h"
#include "DGtal/dec/DiscreteExteriorCalculusFactory.h"

#include "dec_helper.h"

namespace DGtal {

  /**
   * Builds a diagonal linear operator from a k-form. These
   * operators arise naturally when differentiating with respect to
   * another variable (e.g. d/dx (vx)^t (vx) = diag(v^2) x).
   *
   * @param[in] kform any kform w.
   * @return the corresponding linear operator diag(w)
   *
   * @tparam Calculus any discrete exterior calculus.
   * @tparam dim the dimension of the form.
   * @tparam duality either PRIMAL for a primal form or DUAL for a dual form.
   */
  template <typename Calculus, DGtal::Dimension dim, DGtal::Duality duality> 
  DGtal::LinearOperator<Calculus, dim, duality, dim, duality> 
  diagonal(const DGtal::KForm<Calculus, dim, duality>& kform) 
  { 
    typedef DGtal::LinearOperator<Calculus,dim, duality, dim, duality> Operator; 
    typedef typename Calculus::LinearAlgebraBackend::Triplet           Triplet; 
    typedef typename Calculus::Index                                   Index; 
    typedef std::vector<Triplet>                                       Triplets;
    
    Triplets triplets;
    for (Index index=0; index<kform.length(); index++) 
      triplets.push_back(Triplet(index, index, kform.myContainer(index)));
    
    Operator op( kform.myCalculus );
    op.myContainer.setFromTriplets( triplets.begin(), triplets.end() );
    return op;
  }

  
  
  /**
   * Class to solve AT functional on a digital surface.
   */
  template <typename TKSpace>
  struct ATSolver {
    typedef TKSpace                                                    KSpace;
    typedef typename KSpace::Space                                     Space;
    typedef typename Space::RealVector                                 RealVector;
    typedef typename RealVector::Component                             Scalar;
    typedef typename KSpace::SCell                                     SCell;
    typedef typename KSpace::Cell                                      Cell;
    typedef typename KSpace::Surfel                                    Surfel;
    typedef HyperRectDomain<Space>                                     Domain;
    typedef std::map<Cell, double>                                     SmallestEpsilonMap;
    typedef DiscreteExteriorCalculus<2,3, EigenLinearAlgebraBackend>   Calculus;
    typedef DiscreteExteriorCalculusFactory<EigenLinearAlgebraBackend> CalculusFactory;
    typedef Calculus::Index                                            Index;
    typedef Calculus::PrimalForm0                                      PrimalForm0;
    typedef Calculus::PrimalForm1                                      PrimalForm1;
    typedef Calculus::PrimalForm2                                      PrimalForm2;
    typedef Calculus::PrimalIdentity0                                  PrimalIdentity0;
    typedef Calculus::PrimalIdentity1                                  PrimalIdentity1;
    typedef Calculus::PrimalIdentity2                                  PrimalIdentity2;
    typedef Calculus::PrimalDerivative0                                PrimalDerivative0;
    typedef Calculus::PrimalDerivative1                                PrimalDerivative1;
    typedef Calculus::PrimalAntiderivative1                            PrimalAntiderivative1;
    typedef Calculus::PrimalAntiderivative2                            PrimalAntiderivative2;
    typedef Calculus::PrimalHodge0                                     PrimalHodge0;
    typedef Calculus::PrimalHodge1                                     PrimalHodge1;
    typedef Calculus::PrimalHodge2                                     PrimalHodge2;

    // SparseLU is so much faster than SparseQR
    // SimplicialLLT is much faster than SparseLU
    // SimplicialLDLT is as fast as SimplicialLLT but more robust
    // typedef EigenLinearAlgebraBackend::SolverSparseQR LinearAlgebraSolver;
    // typedef EigenLinearAlgebraBackend::SolverSparseLU LinearAlgebraSolver;
    //typedef EigenLinearAlgebraBackend::SolverSimplicialLLT LinearAlgebraSolver;
    typedef EigenLinearAlgebraBackend::SolverSimplicialLDLT LinearAlgebraSolver;
    typedef DiscreteExteriorCalculusSolver<Calculus, LinearAlgebraSolver, 2, PRIMAL, 2, PRIMAL> SolverU2;
    typedef DiscreteExteriorCalculusSolver<Calculus, LinearAlgebraSolver, 0, PRIMAL, 0, PRIMAL> SolverV0;

    const Calculus&     calculus;
    PrimalDerivative0   primal_D0;
    PrimalDerivative1   primal_D1;
    PrimalDerivative0   M01; // point_to_edge average operator
    PrimalDerivative1   M12; // edge_to_face average operator
    PrimalAntiderivative2 primal_AD2;
    PrimalIdentity2     alpha_Id2;
    PrimalIdentity0     l_1_over_4e_Id0;

    std::array<PrimalForm2, 3> g2;
    std::array<PrimalForm2, 3> alpha_g2;
    std::array<PrimalForm2, 3> u2;
    PrimalForm0         v0;
    PrimalForm0         former_v0;
    PrimalForm0         l_1_over_4e;

    double              alpha;
    double              lambda;
    double              epsilon;

    ATSolver( const Calculus& aCalculus )
      : calculus( aCalculus ),
        primal_D0( calculus ), primal_D1( calculus ),
        M01( calculus ), M12( calculus ), primal_AD2( calculus ),
        alpha_Id2( calculus ), l_1_over_4e_Id0( calculus ),
        g2({PrimalForm2(calculus), PrimalForm2(calculus), PrimalForm2(calculus)}),
        alpha_g2({PrimalForm2(calculus), PrimalForm2(calculus), PrimalForm2(calculus)}),
        u2({PrimalForm2(calculus), PrimalForm2(calculus), PrimalForm2(calculus)}),
        v0( calculus ),        former_v0( calculus ),
        l_1_over_4e( calculus )
    {
    }

    Index
    nb(const int order) const
    {
        return calculus.kFormLength(order, PRIMAL);
    }

    void
    init( std::map<SCell,RealVector>& n_estimations )
    {
      trace.beginBlock( "at solver init" );

      trace.info() << calculus << std::endl;
      trace.info() << "primal_D0" << std::endl;
      primal_D0 = calculus.derivative<0,PRIMAL>();
      trace.info() << "primal_D1" << std::endl;
      primal_D1 = calculus.derivative<1,PRIMAL>();
      trace.info() << "primal_AD2" << std::endl;
      primal_AD2 = calculus.antiderivative<2,PRIMAL>();
      //primal_AD2 = primal_D0*dec_helper::generate_face_to_point(calculus);
      //primal_AD2 = primal_D1.transpose();

      trace.info() << "M01" << std::endl;
      M01       = primal_D0;
      M01.myContainer = .5 * M01.myContainer.cwiseAbs();
      trace.info() << "M12" << std::endl;
      M12       = primal_D1;
      M12.myContainer = .25 * M12.myContainer.cwiseAbs();

      trace.info() << "g as 2-form" << std::endl;
      for ( Index index = 0; index < nb(2); index++)
        {
          const SCell&      cell = g2[ 0 ].getSCell( index );
          const RealVector& n    = n_estimations[ cell ];
          g2[ 0 ].myContainer( index ) = n[ 0 ];
          g2[ 1 ].myContainer( index ) = n[ 1 ];
          g2[ 2 ].myContainer( index ) = n[ 2 ];
        }
      // u = g at the beginning
      trace.info() << "u2[0,1,2] = g2[0,1,2]" << std::endl;
      u2 = g2;

      // v = 1 at the beginning
      trace.info() << "v0 = 1" << std::endl;
      v0 = PrimalForm0::ones(calculus);
      trace.endBlock();
    }

    void
    setUp( double a, double l )
    {
      alpha  = a;
      lambda = l;
      alpha_Id2 = alpha*calculus.identity<2, PRIMAL>();
      alpha_g2 = {alpha*g2[0], alpha*g2[1], alpha*g2[2]};
    }

    void
    setUpWithWeightedAlpha( double a, double l, std::map<SCell,Scalar>& weights )
    {
      alpha  = a;
      lambda = l;

      PrimalForm2 w_form( calculus );
      trace.info() << "Using variable weights for fitting (alpha term)" << std::endl;
      alpha_g2 = {alpha*g2[0], alpha*g2[1], alpha*g2[2]};
      for ( Index index = 0; index < nb(2); index++)
        {
          const SCell&           cell = g2[ 0 ].getSCell( index );
          const Scalar&             w = weights[ cell ];
          w_form.myContainer( index ) = w;
          alpha_g2[ 0 ].myContainer( index ) *= w;
          alpha_g2[ 1 ].myContainer( index ) *= w;
          alpha_g2[ 2 ].myContainer( index ) *= w;
        }
      alpha_Id2 = alpha * diagonal( w_form ); //calculus.identity<2, PRIMAL>();
    }

    void
    setEpsilon( double e )
    {
      epsilon     = e;
      l_1_over_4e = lambda/4/epsilon*PrimalForm0::ones(calculus);
      l_1_over_4e_Id0 = lambda/4/epsilon*calculus.identity<0, PRIMAL>();
    }

    // L = A^t A
    // E(u,v) = a(u-g)^t (u-g) +  u^t B diag(M v)^2 B^t u + l e v^t A^t A v + l/(4e) (1-v)^t (1-v)
    // dE/du  = 2( a Id (u-g) + B diag(M v)^2 B^t u )
    //  dE/du = 0 <=> [ a Id + B diag(M v)^2 B^t ] u = a g
    void
    solveU2V0( bool normalize_U2 = false )
    {
      trace.beginBlock("Solving for u as a 2-form");
      PrimalForm1 v1_squared = M01*v0;
      v1_squared.myContainer.array() = v1_squared.myContainer.array().square();
      const PrimalIdentity2 ope_u2 = alpha_Id2 + primal_AD2.transpose()*dec_helper::diagonal(v1_squared)*primal_AD2;

      trace.info() << "Prefactoring matrix U" << std::endl;
      SolverU2 solver_u2;
      solver_u2.compute( ope_u2 );
      for ( unsigned int d = 0; d < 3; ++d )
        {
          trace.info() << "Solving U u[" << d << "] = a g[" << d << "]" << std::endl;
          u2[ d ] = solver_u2.solve( alpha_g2[ d ] );
          trace.info() << "  => " << ( solver_u2.isValid() ? "OK" : "ERROR" )
                       << " " << solver_u2.myLinearAlgebraSolver.info() << std::endl;
        }
      if ( normalize_U2 ) normalizeU2();
      trace.endBlock();
      trace.beginBlock("Solving for v");
      former_v0 = v0;
      PrimalForm1 squared_norm_d_u2 = PrimalForm1::zeros(calculus);
      for ( unsigned int d = 0; d < 3; ++d )
          squared_norm_d_u2.myContainer.array() += (primal_AD2*u2[d]).myContainer.array().square();
      const PrimalIdentity0 ope_v0 = l_1_over_4e_Id0 + lambda*epsilon*primal_D0.transpose()*primal_D0 + M01.transpose()*dec_helper::diagonal(squared_norm_d_u2)*M01;

      trace.info() << "Prefactoring matrix V" << std::endl;
      SolverV0 solver_v0;
      solver_v0.compute( ope_v0 );
      trace.info() << "Solving V v = l/4e * 1" << std::endl;
      v0 = solver_v0.solve( l_1_over_4e );
      trace.info() << "  => " << ( solver_v0.isValid() ? "OK" : "ERROR" )
                   << " " << solver_v0.myLinearAlgebraSolver.info() << std::endl;
      trace.endBlock();
    }

    void
    normalizeU2()
    {
      for ( Index index = 0; index < nb(2); index++)
        {
          double n2 = 0.0;
          for ( unsigned int d = 0; d < 3; ++d )
            n2 += u2[ d ].myContainer( index ) * u2[ d ].myContainer( index );
          double norm = sqrt( n2 );
          if (norm == 0) continue;
          for ( unsigned int d = 0; d < 3; ++d )
            u2[ d ].myContainer( index ) /= norm;
        }
    }

    void
    checkV0()
    {
      const double m1 = v0.myContainer.minCoeff();
      const double m2 = v0.myContainer.maxCoeff();
      const double ma = v0.myContainer.mean();
      trace.info() << "0-form v: min=" << m1 << " avg=" << ma << " max=" << m2 << std::endl;
    }

    void
    diffV0( double& n_infty, double& n_2, double& n_1 )
    {
      PrimalForm0 delta = v0-former_v0;
      delta.myContainer = delta.myContainer.cwiseAbs();

      n_infty = delta.myContainer.maxCoeff();
      n_2 = std::sqrt(delta.myContainer.squaredNorm()/delta.myContainer.size());
      n_1 = delta.myContainer.mean();
    }

    PrimalForm2
    getV2() const
    {
      return M12*M01*v0;
    }

    PrimalForm1
    getV1() const
    {
      return M01*v0;
    }

    void
    updateSmallestEpsilonMap(SmallestEpsilonMap& smallest_eps, const double threshold = .5) const
    {
        const KSpace& K = calculus.myKSpace;
        for ( const SCell face_signed : calculus.getIndexedSCells<2, PRIMAL>() )
        {
            const Cell face            = K.unsigns( face_signed );
            const Dimension    k1      = * K.uDirs( face );
            const Cell   l0      = K.uIncident( face, k1, false );
            const Cell   l1      = K.uIncident( face, k1, true );
            const Dimension    k2      = * K.uDirs( l0 );
            const Cell   ll0     = K.uIncident( face, k2, false );
            const Cell   ll1     = K.uIncident( face, k2, true );
            const Cell   p00     = K.uIncident( l0, k2, false );
            const Cell   p01     = K.uIncident( l0, k2, true );
            const Cell   p10     = K.uIncident( l1, k2, false );
            const Cell   p11     = K.uIncident( l1, k2, true );

            std::vector<double>  features( 4 );
            features[ 0 ] = v0.myContainer( calculus.getCellIndex( p00 ) );
            features[ 1 ] = v0.myContainer( calculus.getCellIndex( p01 ) );
            features[ 2 ] = v0.myContainer( calculus.getCellIndex( p10 ) );
            features[ 3 ] = v0.myContainer( calculus.getCellIndex( p11 ) );
            std::sort( features.begin(), features.end() );

            if ( features[ 1 ] <= threshold )
            {
                auto it = smallest_eps.find( face );
                if ( it != smallest_eps.end() ) it->second = std::min( epsilon, it->second );
                else smallest_eps[ face ] = epsilon;
            }
        }
    }

  };

} // namespace DGtal

#endif // _ATSOLVER_U2_V0_H_
